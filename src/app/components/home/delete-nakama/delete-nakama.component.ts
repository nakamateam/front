import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NakamaService} from '../../../_services/nakama.service';
import {Subscription} from 'rxjs';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-delete-nakama',
  templateUrl: './delete-nakama.component.html',
  styleUrls: ['./delete-nakama.component.scss']
})
export class DeleteNakamaComponent implements OnInit {

  deleteMyNakama: Subscription;

  constructor(public dialogRef: MatDialogRef<DeleteNakamaComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private nakamaService: NakamaService,
              private toastr: ToastrService) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  deleteNakama(nakamaId, nakamaUsername) {
    this.deleteMyNakama = this.nakamaService.deleteMyNakama(nakamaId)
      .subscribe(
        confirmation => {
          if (nakamaUsername) {
            this.deleteNakamaSuccess(nakamaUsername);
          }
          this.dialogRef.close({deleteConfirmation: true});
        },
        error => {
        });
  }

  // Notification to confirm user a mail has been sent
  deleteNakamaSuccess(nakamaUsername) {
    this.toastr.success( 'You are no longer nakama with ' + nakamaUsername, '', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 4000,
    });
  }

}
