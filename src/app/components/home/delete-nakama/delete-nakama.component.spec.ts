import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteNakamaComponent } from './delete-nakama.component';

describe('DeleteNakamaComponent', () => {
  let component: DeleteNakamaComponent;
  let fixture: ComponentFixture<DeleteNakamaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteNakamaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteNakamaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
