import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatBottomSheet, MatDialog, MatSort, Sort} from '@angular/material';
import {CreateGroupComponent} from './create-group/create-group.component';
import {NakamaService} from '../../_services/nakama.service';
import {Subscription} from 'rxjs';
import {NotificationService} from '../../_services/notification.service';
import {DeleteNakamaComponent} from './delete-nakama/delete-nakama.component';
import {NakamaDetailDialogComponent} from '../../nakama-detail-dialog/nakama-detail-dialog.component';
import {Notifications, NOTIFTYPES} from '../../_models';
import {GroupService} from '../../_services/group.service';
import {DeleteGroupComponent} from './delete-group/delete-group.component';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {WebSocketService} from '../../_services/web-socket.service';
import {GroupUsersBottomSheetComponent} from './group-users-bottom-sheet/group-users-bottom-sheet.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  getMyNakamaService: Subscription;
  getMyGroupsService: Subscription;
  receiveNewMessageNotif: Subscription;
  acceptNakamaService: Subscription;
  getMyFriendNotifService: Subscription;
  getConnectedUsersService: Subscription;
  getUnreadMessageService: Subscription;
  nakamaLoaded = false;
  notifType = NOTIFTYPES;
  nakamaLoadedError: Boolean = false;
  groupLoaded = false;
  groupLoadedError: Boolean = false;
  nakamas = [];
  groups = [];
  sortedData: any[];
  nakamaPendingRequest = [];
  myConnectedNakamas: Subscription;
  activeNakamaUsers = [];
  mySelectedWay = 'myWay';
  @ViewChild('deleteNakama') private buttonDeleteNakama: ElementRef;
  @ViewChild('nakamaWayModalButton') private buttonCreateNakamaway: ElementRef;
  @Input()selectedIndex: number | null;
  indexAuto: Number = 1;
  sortInit: Sort = { active: 'connection', direction: 'desc' };
  static compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  constructor(public dialog: MatDialog,
              private nakamaService: NakamaService,
              private notificationService: NotificationService,
              private webSocketService: WebSocketService,
              private groupService: GroupService,
              private toastr: ToastrService,
              private router: Router,
              private bottomSheet: MatBottomSheet) {
  }

  ngOnInit() {
    document.body.classList.remove('nakamaLoGif');
    document.body.classList.add('greyBackground');
    this.getMyNakamas();
    this.getMyGroups();
    this.updateMyLiveNotifications();
    this.updateConnectedUsers();
    this.updateNewMessagesByGroup();
  }

  sortData(sort: Sort) {
    const data = this.groups.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return HomeComponent.compare(a.groupName, b.groupName, isAsc);
        case 'connection': return HomeComponent.compare(a.lastConnectionDate, b.lastConnectionDate, isAsc);
        default: return 0;
      }
    });
  }

  getMyNakamas() {
    this.nakamaPendingRequest = [];
    this.getMyNakamaService = this.nakamaService.getMyNakamas()
      .subscribe(
        nakamas => {
          console.log(nakamas);
          nakamas.forEach((nakama) => {
            if (nakama.status === 'accepted') {
              this.nakamas.push(nakama);
            }
            if (nakama.status === 'pending') {
              this.nakamaPendingRequest.push(nakama);
            }
          });
          if (this.activeNakamaUsers.length > 0) {
            this.updateConnectedUsers();
          }
          this.refreshMyConnectedNakamas();
          this.nakamaLoaded = true;
          if (this.groupLoaded && this.groups.length > 0) {
            this.indexAuto = 1;
          } else if (this.groupLoaded && this.groups.length === 0 && this.nakamas.length === 0) {
            this.indexAuto = 0;
          }
        },
        error => {
          this.nakamaLoadedError = true;
        });
  }

  getMyGroups() {
    this.groupLoaded = false;
    this.groups = [];
    this.getMyGroupsService = this.groupService.getMyGroups()
      .subscribe(
        groups => {
          console.log(groups);
          this.groups = groups;
          this.sortedData = this.groups.slice();
          this.sortData(this.sortInit);
          this.sortedData.forEach((group, i) => {
            this.getGroupConnectedUsers(group._id, group);
            this.getGroupUnreadMessage(group._id, group);
          });
          this.groupLoaded = true;
          if (groups.length > 0) {
            this.indexAuto = 1;
          } else if (this.nakamaLoaded && this.nakamas.length === 0) {
            this.indexAuto = 0;
          }
        },
        error => {
          this.groupLoadedError = true;
        });
  }

  getGroupConnectedUsers(roomId, group) {
    console.log('inside getGroupConnectedUsers');
    this.webSocketService.knoknokWosZer(roomId);
    this.getConnectedUsersService = this.webSocketService.roomConnectedUsersByRoomId(roomId).subscribe(data => {
      console.log('PerEachRoomConnectedUsers!');
      console.log(data);
      group.connectedUsers = data;
    });
  }

  getGroupUnreadMessage(roomId, group) {
    console.log('inside getGroupUnreadMessage');
    this.getUnreadMessageService = this.notificationService.getMyUnreadMessage(roomId).subscribe(data => {
      console.log(data);
      group.unreadMessage = data;
    });
  }

  updateNakamaRequestList() {
    this.nakamaPendingRequest = [];
    this.getMyNakamaService = this.nakamaService.getMyNakamas()
      .subscribe(
        nakamas => {
          this.resetLiveNotif();
          nakamas.forEach((nakama) => {
            if (nakama.status === 'pending') {
              this.nakamaPendingRequest.push(nakama);
            }
          });
        },
        error => {
        });
  }

  updateNakamaList() {
    this.nakamas = [];
    this.getMyNakamaService = this.nakamaService.getMyNakamas()
      .subscribe(
        nakamas => {
          this.resetLiveNotif();
          nakamas.forEach((nakama) => {
            if (nakama.status === 'accepted') {
              this.nakamas.push(nakama);
            }
          });
          if (this.activeNakamaUsers.length > 0) {
            this.updateConnectedUsers();
          }
        },
        error => {
        });
  }

  updateGroupList() {
    this.groups = [];
    this.getMyGroupsService = this.groupService.getMyGroups()
      .subscribe(
        groups => {
          this.resetLiveNotif();
          this.groups = groups;
          this.groupLoaded = true;
        },
        error => {
          this.groupLoaded = true;
        });
  }

  resetLiveNotif() {
    this.notificationService.changeLiveNotif(NOTIFTYPES.INITIATENOTIF);
  }

  updateMyLiveNotifications() {
    this.getMyFriendNotifService = this.notificationService.liveNotif.subscribe((notifType) => {
      if (notifType === NOTIFTYPES.NAKAMAREQUEST) {
        console.log('Jai un nakamarequ');
        this.updateNakamaRequestList();
      }
      if (notifType === NOTIFTYPES.NEWNAKAMACONFIRMATION) {
        console.log('Jai un nakamaconf');
        this.updateNakamaList();
      }
      if (notifType === NOTIFTYPES.NEWGROUPINVITATION) {
        console.log('Jai un newGroupInvitation');
        this.updateGroupList();
      }
      if (notifType === NOTIFTYPES.REQUESTCANCELED) {
        console.log('oups, I lost a potential Nakama');
        this.updateNakamaRequestList();
      }
    });
  }

  updateConnectedUsers() {
    if (!this.myConnectedNakamas) {
      this.myConnectedNakamas = this.webSocketService.myConnectedNakamas.subscribe((myConnectedNakamasArray) => {
        this.resetConnectedUsers();
        console.log('inside updateConnectedUsers');
        console.log(myConnectedNakamasArray);
        this.activeNakamaUsers = myConnectedNakamasArray;
        this.nakamas.forEach(function (nakama) {
          myConnectedNakamasArray.forEach(function (connectedNakamaId) {
            if (nakama._id === connectedNakamaId) {
              nakama.isconnected = true;
            }
          });
        });
      });
    }
  }

  updateNewMessagesByGroup() {
    this.receiveNewMessageNotif = this.webSocketService.onNotif()
      .subscribe((Notification: Notifications) => {
        this.handleNewMessage(Notification);
        console.log(Notification);
      });
  }

  handleNewMessage(newMessageNotif: Notifications) {
    if (newMessageNotif.notificationType === this.notifType.NEWMESSAGE) {
      const index = this.sortedData.findIndex(group => group._id === newMessageNotif.metaData.groupId);
      if (index !== -1) {
        this.sortedData[index].unreadMessage++;
      }
    }
  }

  refreshMyConnectedNakamas() {
    this.webSocketService.refreshConnectedUserList();
  }

  resetConnectedUsers() {
    this.nakamas.forEach(function(nakama) {
        delete nakama.isconnected;
      }
    );
  }

  acceptNakama(nakId) {
    this.acceptNakamaService = this.nakamaService.acceptNakamaRequest(nakId)
      .subscribe(
        users => {
          this.nakamas = [];
          this.nakamaPendingRequest = [];
          this.getMyNakamas();
        },
        error => {
          console.log(error);
          if (error.status === 403) {
            this.updateNakamaRequestList();
            this.nakamaRequestNoLongerValid();
          }
        });
  }

  public reloadNakama(confirmation: any): void {
    if (confirmation) {
      this.nakamas = [];
      this.nakamaPendingRequest = [];
      this.getMyNakamas();
    }
  }

  joinMyWay(id) {
    this.groupService.changeGroupId(id);
    localStorage.setItem(this.mySelectedWay, id);
    this.router.navigate(['way/chat']);
  }

  joinMyWayNotificationSettings(id) {
    this.groupService.changeGroupId(id);
    localStorage.setItem(this.mySelectedWay, id);
    this.router.navigate(['way/notifications']);
  }

  joinMyWayGroupSettings(id) {
    this.groupService.changeGroupId(id);
    localStorage.setItem(this.mySelectedWay, id);
    this.router.navigate(['way/settings']);
  }

  openGroupCreationDialog(): void {
    const dialogRef = this.dialog.open(CreateGroupComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'nakamaListTransparant',
      data: {nakamas: this.nakamas}
    });
    dialogRef.afterClosed().subscribe(group => {
      this.fixButtonRippleEffectBug(this.buttonCreateNakamaway);
      if (group) {
        this.joinMyWay(group.newGroup._id);
      }
    });
  }

  openDeleteNakamaDialog(nakamId, nakamaUsername): void {
    const dialogRef = this.dialog.open(DeleteNakamaComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {nakamaId: nakamId, nakamaUsername: nakamaUsername}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      this.fixButtonRippleEffectBug(this.buttonDeleteNakama);
      if (confirm && confirm.deleteConfirmation) {
        this.nakamas = [];
        this.nakamaPendingRequest = [];
        this.getMyNakamas();
      }
    });
  }

  openDeleteGroupDialog(groupId, groupName, isAdmin): void {
    const dialogRef = this.dialog.open(DeleteGroupComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {groupId: groupId, groupName: groupName, isAdmin: isAdmin}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm && confirm.deleteConfirmation) {
        this.getMyGroups();
      }
    });
  }

  openNakamaDetailDialog(avatarUrl): void {
    const dialogRef = this.dialog.open(NakamaDetailDialogComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {avatarUrl: avatarUrl}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      // this.fixButtonRippleEffectBug(this.buttonDeleteNakama);
    });
  }

  openGroupMembers(group): void {
    this.bottomSheet.open(GroupUsersBottomSheetComponent,
      {
        data: { group: group },
        restoreFocus: false
      });
  }

  fixButtonRippleEffectBug(button) {
    button._elementRef.nativeElement.classList.remove('cdk-program-focused');
    button._elementRef.nativeElement.classList.add('cdk-mouse-focused');
  }

  ngOnDestroy(): void {
    document.body.classList.remove('greyBackground');
    if (this.acceptNakamaService) {
      this.acceptNakamaService.unsubscribe();
    }
    if (this.getMyFriendNotifService) {
      this.getMyFriendNotifService.unsubscribe();
    }
    if (this.getConnectedUsersService) {
      this.getConnectedUsersService.unsubscribe();
    }
    if (this.myConnectedNakamas) {
      this.myConnectedNakamas.unsubscribe();
    }
    if (this.getUnreadMessageService) {
      this.getUnreadMessageService.unsubscribe();
    }
    if (this.receiveNewMessageNotif) {
      this.receiveNewMessageNotif.unsubscribe();
    }
      this.getMyGroupsService.unsubscribe();
      this.getMyNakamaService.unsubscribe();
  }

  // Notification in case a pending request is no longer valid due to a cancellation
  nakamaRequestNoLongerValid() {
    this.toastr.error( 'Sorry, it seems this pending request is no longer valid', '', {
      closeButton: true,
      positionClass: 'toast-bottom-center',
      timeOut: 4000,
    });
  }
}
