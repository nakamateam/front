import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchNakamaComponent } from './search-nakama.component';

describe('SearchNakamaComponent', () => {
  let component: SearchNakamaComponent;
  let fixture: ComponentFixture<SearchNakamaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchNakamaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchNakamaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
