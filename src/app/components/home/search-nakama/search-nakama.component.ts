import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {debounceTime, distinctUntilChanged, map, startWith, switchMap} from 'rxjs/internal/operators';
import {Observable, Subscription} from 'rxjs';
import {NakamaService} from '../../../_services/nakama.service';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../../_models';
import {NakamaDetailDialogComponent} from '../../../nakama-detail-dialog/nakama-detail-dialog.component';
import {MatAutocompleteTrigger, MatDialog} from '@angular/material';

@Component({
  selector: 'app-search-nakama',
  templateUrl: './search-nakama.component.html',
  styleUrls: ['./search-nakama.component.scss']
})
export class SearchNakamaComponent implements OnInit, OnDestroy {

  getNewNakamasService: Subscription;
  acceptNakamaService: Subscription;
  cancelMyRequest: Subscription;
  addNewNakamaService: Subscription;
  @Output() onRequestAccepted: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild(MatAutocompleteTrigger) inputAutoComplit: MatAutocompleteTrigger;

  myControl = new FormControl();
  filteredOptions: Observable<User[]>;
  rawfilteredOptions: User[];

  constructor(private nakamaService: NakamaService,
              private toastr: ToastrService,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        debounceTime(400),
        distinctUntilChanged(),
        switchMap(val => {
            return this.filter(val || '');
        })
      );

    this.getNewNakamasService = this.filteredOptions
      .subscribe(mySearchlist => {
          console.log(mySearchlist);
          this.rawfilteredOptions = mySearchlist;
        },
        error => {
        });
  }

  // filter and return the values
  filter(val: string): Observable<User[]> {
    return this.nakamaService.search(val)
      .pipe(
        map(response => response.filter(option => {
          if (val.length >= 1) {
            return option;
          }
        }))
      );
  }

  displayFn(): string | undefined {
    return '';
  }

  sendNakamaRequest(nakId, username) {
    this.addNewNakamaService = this.nakamaService.sendNakamaRequest(nakId)
      .subscribe(
        users => {
          this.showNakamaAddSuccess(username);
          console.log(this.myControl.value);
          this.filteredOptions = this.myControl.valueChanges
            .pipe(
              startWith(''),
              switchMap(val => {
                return this.filter(this.myControl.value);
              })
            );

          this.getNewNakamasService = this.filteredOptions
            .subscribe(mySearchlist => {
                console.log(mySearchlist);
                this.rawfilteredOptions = mySearchlist;
              },
              error => {
              });

        },
        error => {
        });
  }

  acceptNakamaRequest(nakId) {
    this.acceptNakamaService = this.nakamaService.acceptNakamaRequest(nakId)
      .subscribe(
        users => {
          this.onRequestAccepted.emit(true);

          this.filteredOptions = this.myControl.valueChanges
            .pipe(
              startWith(''),
              switchMap(val => {
                return this.filter(this.myControl.value);
              })
            );

          this.getNewNakamasService = this.filteredOptions
            .subscribe(mySearchlist => {
                console.log(mySearchlist);
                this.rawfilteredOptions = mySearchlist;
              },
              error => {
              });

        },
        error => {
        });
  }

  cancelRequest(nakamaId, nakamaUsername) {
    this.cancelMyRequest = this.nakamaService.cancelRequestAndUpdateNotif(nakamaId)
      .subscribe(
        confirmation => {

          this.filteredOptions = this.myControl.valueChanges
            .pipe(
              startWith(''),
              switchMap(val => {
                return this.filter(this.myControl.value);
              })
            );

          this.getNewNakamasService = this.filteredOptions
            .subscribe(mySearchlist => {
                console.log(mySearchlist);
                this.rawfilteredOptions = mySearchlist;
              },
              error => {
              });

          if (nakamaUsername) {
            this.deleteNakamaSuccess(nakamaUsername);
          }
        },
        error => {
        });
  }

  // Notification to confirm user new nakama
  showNakamaAddSuccess(newNakamaUsername) {
    this.toastr.success('A new Nakama Request was sent to ' + newNakamaUsername, '', {
      closeButton: true,
      positionClass: 'toast-bottom-center',
      timeOut: 4000,
    });
  }

  deleteNakamaSuccess(nakamaUsername) {
    this.toastr.success( 'Your friend request with ' + nakamaUsername + ' has been canceled', '', {
      closeButton: true,
      positionClass: 'toast-bottom-center',
      timeOut: 4000,
    });
  }

  openNakamaDetailDialog(avatarUrl): void {
    const dialogRef = this.dialog.open(NakamaDetailDialogComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {avatarUrl: avatarUrl}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      this.inputAutoComplit.openPanel();
      // this.fixButtonRippleEffectBug(this.buttonDeleteNakama);
    });
  }

  ngOnDestroy(): void {
    if (this.getNewNakamasService) {
      this.getNewNakamasService.unsubscribe();
    }
    if (this.cancelMyRequest) {
      this.cancelMyRequest.unsubscribe();
    }
    if (this.addNewNakamaService) {
      this.addNewNakamaService.unsubscribe();
    }
  }

}
