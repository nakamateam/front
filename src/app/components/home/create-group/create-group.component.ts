import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {concat, Observable, of, Subject, Subscription} from 'rxjs';
import {User} from '../../../_models';
import {catchError, debounceTime, delay, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {NakamaService} from '../../../_services/nakama.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {GroupService} from '../../../_services/group.service';
import {Group, UserGroup} from '../../../_models';

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.scss']
})
export class CreateGroupComponent implements OnInit, OnDestroy {
  people3$: Observable<User[]>;
  people3Loading = false;
  people3input$ = new Subject<string>();
  selectedPersons: User[] = <any>[];
  getMyNakamaService: Subscription;
  nakamas = [];

  newNakaGroupForm = new FormGroup({
    newNakamaGroupName: new FormControl('', [
      Validators.required,
      Validators.maxLength(50),
      Validators.minLength(1),
    ]),
  });

  createNewGroupService: Subscription;
  dataLoaded = true;

  get groupName() { return this.newNakaGroupForm.get('newNakamaGroupName');  }


  constructor(public dialogRef: MatDialogRef<CreateGroupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private nakamaService: NakamaService,
              private groupService: GroupService) { }

  ngOnInit() {
    this.loadPeople3();
    this.nakamas = this.data.nakamas;
  }

  getMyNakamas() {
    this.getMyNakamaService = this.nakamaService.getMyAcceptedNakama()
      .subscribe(
        nakamas => {
          this.nakamas = nakamas;
        },
        error => {
        });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  private loadPeople3() {
    this.people3$ = concat(
      of(this.data.nakamas), // default items
      this.people3input$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => this.people3Loading = true),
        switchMap(term => this.getNakamas(term).pipe(
          catchError(() => of([])), // empty list on error
          tap(() => this.people3Loading = false)
        ))
      )
    );
  }

  newGroup() {
    this.dataLoaded = false;
    const group = new Group();
    group.groupName = this.newNakaGroupForm.get('newNakamaGroupName').value;
    console.log(this.selectedPersons);
    this.selectedPersons.forEach(function (selectedPerson) {
      const participant = new UserGroup(selectedPerson._id);
      group.users.push(participant);
    });
    this.createNewGroupService = this.groupService.create(group)
      .subscribe(
        newGroup => {
          console.log(newGroup);
          this.dataLoaded = true;
          this.dialogRef.close({newGroup: newGroup});
        },
        error => {
          this.dataLoaded = true;
        }
      );
  }

  getNakamas(term: string = null): Observable<User[]> {
    let items = this.nakamas;
    if (term) {
      items = items.filter(x =>
        x.friend.username.toLocaleLowerCase().indexOf(term.toLocaleLowerCase()) > -1 ||
        x.friend.firstName.toLocaleLowerCase().indexOf(term.toLocaleLowerCase()) > -1 ||
        x.friend.lastName.toLocaleLowerCase().indexOf(term.toLocaleLowerCase()) > -1);
    }
    return of(items).pipe(delay(10));
  }

  ngOnDestroy(): void {
    if (this.getMyNakamaService) {
      this.getMyNakamaService.unsubscribe();
    }
    if (this.createNewGroupService) {
      this.createNewGroupService.unsubscribe();
    }
  }

}
