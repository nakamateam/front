import {Component, Inject, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {GroupService} from '../../../_services/group.service';

@Component({
  selector: 'app-delete-group',
  templateUrl: './delete-group.component.html',
  styleUrls: ['./delete-group.component.scss']
})
export class DeleteGroupComponent implements OnInit {

  deleteGroup: Subscription;


  constructor(public dialogRef: MatDialogRef<DeleteGroupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private groupService: GroupService,
              private toastr: ToastrService) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  leaveWay(groupId, groupName) {
    this.deleteGroup = this.groupService.deleteGroup(groupId)
      .subscribe(
        confirmation => {
            this.leaveGroupSuccess(groupName);
          this.dialogRef.close({deleteConfirmation: true});
        },
        error => {
        });
  }

  // Notification to confirm user a mail has been sent
  leaveGroupSuccess(groupName) {
    this.toastr.success( 'You are no longer part of the Nakama Way: ' + groupName, '', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 4000,
    });
  }

}
