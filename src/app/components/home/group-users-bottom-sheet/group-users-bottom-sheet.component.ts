import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatDialog} from '@angular/material';

@Component({
  selector: 'app-group-users-bottom-sheet',
  templateUrl: './group-users-bottom-sheet.component.html'
})
export class GroupUsersBottomSheetComponent implements OnInit {

  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
              public dialog: MatDialog) { }

  ngOnInit() {
  }

  reRender() {
  }

}
