import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {GroupService} from '../../../_services/group.service';

@Component({
  selector: 'app-group-title',
  templateUrl: './group-title.component.html',
  styleUrls: ['./group-title.component.scss']
})
export class GroupTitleComponent implements OnInit {
  updateNakamGroupService: Subscription;
  currentGroupName = null;

  constructor(private groupService: GroupService) { }

  ngOnInit() {
    this.updateGroupTitle();
  }

  updateGroupTitle() {
    this.updateNakamGroupService = this.groupService.currentGroupName.subscribe((wayTitle) => {
      this.currentGroupName = wayTitle;
    }, error => {
    });
  }

}
