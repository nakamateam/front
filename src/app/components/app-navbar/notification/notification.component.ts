import {Component, OnDestroy, OnInit} from '@angular/core';
import {NotificationService} from '../../../_services/notification.service';
import {Notifications, NOTIFTYPES} from '../../../_models';
import {Subscription} from 'rxjs';
import {MatDialog} from '@angular/material';
import {NakamaDetailDialogComponent} from '../../../nakama-detail-dialog/nakama-detail-dialog.component';
import {environment} from '../../../../environments/environment';
import {GroupService} from '../../../_services/group.service';
import {Router} from '@angular/router';
import {FileService} from '../../../_services/file.service';
import {faFile, faFilePdf} from '@fortawesome/free-solid-svg-icons';
import {EventService} from '../../../_services/event.service';
import {ToastrService} from 'ngx-toastr';
import {take} from 'rxjs/operators';
import {WebSocketService} from '../../../_services/web-socket.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit, OnDestroy {

  ioConnection: any;
  receiveNotif: any;
  receiveEventAlertNotif: Subscription;
  activSocketUsers: any[] = [];
  getMyNotification: Subscription;
  updateMyNotification: Subscription;
  notifications: Notifications[] = [];
  messageNotification: Notifications[] = [];
  notifType = NOTIFTYPES;
  notificationStatus = [];
  messageUnreadNotif = 0;
  notifNumber = 15;
  myConnectedNakamas: any;
  newNakamaConnection: any;
  newNakamaDisonnection: any;
  onNetworkError: Subscription;
  activeNakamaUsers = [];
  dataloaded = false;
  docRootUrl = environment.serverHttp + '/file/';
  mySelectedWay = 'myWay';
  faFile = faFile;

  constructor(private NotifService: NotificationService,
              private webSocketService: WebSocketService,
              public dialog: MatDialog,
              private groupService: GroupService,
              private router: Router,
              private fileService: FileService,
              private eventService: EventService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.initIoConnection();
    this.getMyNotifications(this.notifNumber);
  }

  private initIoConnection(): void {
    this.webSocketService.initSocket();
    this.webSocketService.send('charlisbak bakagain');

    this.ioConnection = this.webSocketService.updateConnectedUsers()
      .subscribe((activSocketUsers: any) => {
        this.activSocketUsers = activSocketUsers;
        console.log(activSocketUsers);
      });

    this.myConnectedNakamas = this.webSocketService.getMyConnectedNakamaOnConnect()
      .subscribe((activNakama: any) => {
        console.log('inside getMyConnectedNakamaOnConnect');
        console.log(activNakama);
        this.activeNakamaUsers = activNakama;
        this.webSocketService.updateMyConnectedNakamas(this.activeNakamaUsers);
      });

    this.newNakamaConnection = this.webSocketService.updateConnectedNakamas()
      .subscribe((newActiveNakama: any) => {
        console.log('inside updateConnectedNakamas');
        console.log(newActiveNakama);
        if (this.activeNakamaUsers.find(nakId => nakId === newActiveNakama.toString()) === undefined) {
          this.activeNakamaUsers.push(newActiveNakama);
          this.webSocketService.updateMyConnectedNakamas(this.activeNakamaUsers);
        }
      });

    this.newNakamaDisonnection = this.webSocketService.updateDisonnectedNakamas()
      .subscribe((leavingNakama: any) => {
        console.log('inside updateDisonnectedNakamas');
        const index = this.activeNakamaUsers.indexOf(leavingNakama);
        if (index !== -1) {
          this.activeNakamaUsers.splice(index, 1);
        }
        this.webSocketService.updateMyConnectedNakamas(this.activeNakamaUsers);
        console.log(this.activeNakamaUsers);
      });

    this.onNetworkError = this.webSocketService.resetActiveNakamaUsersList()
      .subscribe((resetList: any) => {
        console.log('inside resetActiveNakamaUsersList onNetworkError');
        this.activeNakamaUsers = [];
        this.webSocketService.updateMyConnectedNakamas(this.activeNakamaUsers);
        console.log(this.activeNakamaUsers);
      });

    this.receiveNotif = this.webSocketService.onNotif()
      .subscribe((Notification: Notifications) => {
        this.handleLiveNotif(Notification);
        console.log(Notification);
      });

    this.receiveEventAlertNotif = this.webSocketService.onEventAlertNotif()
      .subscribe((Notification: Notifications) => {
        console.log(Notification);
        this.showEventAlertToastr(Notification);
      });
  }

  handleLiveNotif(liveNotif) {
    if (liveNotif.notificationType === this.notifType.NEWMESSAGE) {
      this.notifications.unshift(liveNotif);
      this.messageNotification.push(liveNotif);
      this.messageUnreadNotif++;
    } else {
      this.notifications = [];
      this.getMyNotifications(this.notifNumber);
      this.NotifService.changeLiveNotif(liveNotif.notificationType);
    }
  }

  towardWay(notif: Notifications) {
    console.log('towoard rayyyyyyyyy');
    const fileService = this.fileService;
    const eventService = this.eventService;
    this.groupService.changeGroupId(notif.metaData.groupId);
    localStorage.setItem(this.mySelectedWay, notif.metaData.groupId.toString());
    switch (notif.notificationType) {
      case this.notifType.NEWGROUPINVITATION:
        this.router.navigate(['way/chat'])
          .then(function (safeAndSound) {
          });
        break;
      case this.notifType.NEWMESSAGE:
        this.router.navigate(['way/chat'])
          .then(function (safeAndSound) {
          });
        break;
      case this.notifType.NEWFILEUPLOADED:
        this.router.navigate(['way/sharedocs'])
          .then(function (safeAndSound) {
            fileService.changeGroupFileId(notif.metaData.fileId);
          });
        break;
      case this.notifType.NEWGROUPNAME:
        this.router.navigate(['way/settings'])
          .then(function (safeAndSound) {
          });
        break;
      case this.notifType.NEWGROUPAVATAR:
        this.router.navigate(['way/settings'])
          .then(function (safeAndSound) {
          });
        break;
      case this.notifType.NEWCALENDAREVENT:
        this.router.navigate(['way/calendar'])
          .then(function (safeAndSound) {
            eventService.changeGroupEventId(notif.metaData.eventId);
          });
        break;
      case this.notifType.CALENDAREVENTEDITED:
        this.router.navigate(['way/calendar'])
          .then(function (safeAndSound) {
            eventService.changeGroupEventId(notif.metaData.eventId);
          });
        break;
      default:
        this.router.navigate(['way/chat'])
          .then(function (safeAndSound) {
          });
    }
  }

  getMyNotifications(notifNumber) {
    this.notificationStatus = [];
    const me = this;
    this.getMyNotification = this.NotifService.getMyNotifications(notifNumber)
      .subscribe(
        notifications => {
          console.log(notifications);
          this.notifications = notifications.concat(this.messageNotification);
          this.notifications.sort((val1, val2) =>  val1.created_at < val2.created_at ? 1 : -1);
          this.notifications.forEach(function (notif) {
            notif.timeSinceNotif = me.getNotifDateFormat(notif.created_at);
          });
          this.handleNotifBadge(notifications);
          this.dataloaded = true;
        },
        error => {
        });
  }

  handleNotifBadge(notifications) {
    const notifObject = this;
    notifications.forEach(function (notif) {
      if (notif.unRead) {
        notifObject.notificationStatus.push(notif._id);
      }
    });
  }

  menuOpened() {
    const me = this;
    this.notifications.forEach(function (notif) {
      notif.timeSinceNotif = me.getNotifDateFormat(notif.created_at);
    });
  }

  showEventAlertToastr(notification: Notifications) {
    console.log(notification);
    this.toastr.info('<div class="towardEvent">' +
      'You have a new event starting ' + notification.metaData.alertType + '</div>' +
      '<img class="groupAvatarToastr" alt="nakamaPicture" src=' + notification.groupInfo.avatarUrl + '>',
      notification.metaData.eventTitle, {
        closeButton: true,
        enableHtml: true,
        titleClass: 'evenTitleToast',
        positionClass: 'toast-bottom-left',
        disableTimeOut: true,
      }).onTap
      .pipe(take(1))
      .subscribe(() => this.towardWay(notification));
  }

  menuClosed() {
    this.messageNotification.forEach(notif => {
      notif.unRead = false;
    });
    this.messageUnreadNotif = 0;
    if (this.notificationStatus.length > 0) {
        this.updateMyNotification = this.NotifService.updateNotifications(this.notificationStatus)
          .subscribe(
            notifications => {
              this.notificationStatus = [];
              this.getMyNotifications(this.notifNumber);
            },
            error => {
            });
    }
  }

  openNakamaDetailDialog(avatarUrl): void {
    const dialogRef = this.dialog.open(NakamaDetailDialogComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {avatarUrl: avatarUrl}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      // this.fixButtonRippleEffectBug(this.buttonDeleteNakama);
    });
  }

  getNotifDateFormat(notifDate) {
    const currenTime = Date.parse(new Date().toISOString());
    const notifCreationDate = Date.parse(notifDate);
    const timeDifferenceInMs = currenTime - notifCreationDate;

    const diffInSecond = (timeDifferenceInMs / 1000);
    const diffInMinute = (timeDifferenceInMs / (1000 * 60));
    const diffInHours = (timeDifferenceInMs / (1000 * 60 * 60));
    const diffInDays = (timeDifferenceInMs / (1000 * 60 * 60 * 24));
    const diffInweeks = (timeDifferenceInMs / (1000 * 60 * 60 * 24 * 7));
    const diffInMonth = (timeDifferenceInMs / (1000 * 60 * 60 * 24 * 30));
    const diffInYear = (timeDifferenceInMs / (1000 * 60 * 60 * 24 * 365));
    if (diffInSecond < 60) {
      return Math.round(diffInSecond) + 's';
    } else if (diffInMinute < 60) {
      return Math.round(diffInMinute) + 'm';
    } else if (diffInHours < 24) {
      return Math.round(diffInHours) + 'h';
    } else if (diffInDays < 7) {
      return Math.round(diffInDays) + 'd';
    } else if (diffInweeks < 4) {
      return Math.round(diffInweeks) + 'w';
    } else if (diffInMonth < 12) {
      return Math.round(diffInMonth) + 'month';
    } else if (diffInYear < 12) {
      return Math.round(diffInYear) + 'y';
    }
  }

  onBottomReached(event) {
    const elem = document.getElementById('notifs');
    const pos = elem.scrollTop + elem.offsetHeight;
    const max = elem.scrollHeight;
    if (Math.round(pos) === (max)) {
      if (this.notifNumber <= this.notifications.length) {
        this.notifNumber = this.notifNumber + 10;
        this.getMyNotifications(this.notifNumber);
      }
    }
  }

  ngOnDestroy(): void {
    if (this.getMyNotification) {
      this.getMyNotification.unsubscribe();
    }
    if (this.updateMyNotification) {
      this.updateMyNotification.unsubscribe();
    }
    if (this.receiveNotif) {
      this.receiveNotif.unsubscribe();
    }
    if (this.ioConnection) {
      this.ioConnection.unsubscribe();
    }
    if (this.myConnectedNakamas) {
      this.myConnectedNakamas.unsubscribe();
    }
    if (this.newNakamaConnection) {
      this.newNakamaConnection.unsubscribe();
    }
    if (this.newNakamaDisonnection) {
      this.newNakamaDisonnection.unsubscribe();
    }
    if (this.receiveEventAlertNotif) {
      this.receiveEventAlertNotif.unsubscribe();
    }
    if (this.onNetworkError) {
      this.onNetworkError.unsubscribe();
    }
  }

}
