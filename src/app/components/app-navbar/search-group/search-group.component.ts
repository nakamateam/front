import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Group} from '../../../_models/group';
import {debounceTime, distinctUntilChanged, map, startWith, switchMap} from 'rxjs/internal/operators';
import {GroupService} from '../../../_services/group.service';
import {User} from '../../../_models';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-search-group',
  templateUrl: './search-group.component.html',
  styleUrls: ['./search-group.component.scss']
})
export class SearchGroupComponent implements OnInit {
  @ViewChild('groupResults') groupChoices: ElementRef;
  getMyGroups: Subscription;
  groupControl = new FormControl();
  filteredGroupOptions: Observable<Group[]>;
  myGroupList = [];
  mySelectedWay = 'myWay';
  @HostListener('document:click', ['$event']) clickedOutside($event) {
    if (this.groupChoices) {
      this.groupChoices.nativeElement.classList.add('hideResults');
    }
  }

  constructor(private groupService: GroupService,
              private router: Router) {
    this.filteredGroupOptions = this.groupControl.valueChanges
      .pipe(
      startWith(''),
      debounceTime(400),
      distinctUntilChanged(),
      switchMap(val => {
        return this.filter(val || '');
      })
    );

    this.getMyGroups = this.filteredGroupOptions
      .subscribe(myGroupList => {
          console.log(myGroupList);
          this.myGroupList = myGroupList;
        },
        error => {
        });
  }

  ngOnInit() {}

  // filter and return the values
  filter(val: string): Observable<Group[]> {
    return this.groupService.search(val)
      .pipe(
        map(response => response.filter(option => {
          if (val.length >= 1) {
            return option;
          }
        }))
      );
  }

  clickedInsideInput($event: Event) {
    $event.preventDefault();
    $event.stopPropagation();
    if (this.groupChoices) {
      this.groupChoices.nativeElement.classList.remove('hideResults');
    }
  }

  clickedInside($event: Event, groupId, groupName) {
    console.log(this.router.url);
    localStorage.setItem(this.mySelectedWay, groupId);
    $event.preventDefault();
    $event.stopPropagation();  // <- that will stop propagation on lower layers
    this.groupControl.setValue('');
    this.groupService.changeGroupId(groupId);
    this.groupChoices.nativeElement.classList.add('hideResults');
    switch (this.router.url) {
      case '/way/sharedocs':
        this.router.navigate(['way/sharedocs'])
          .then(function (safeAndSound) {
            console.log(safeAndSound);
          });
        break;
      case '/way/calendar':
        this.router.navigate(['way/calendar'])
          .then(function (safeAndSound) {
            console.log(safeAndSound);
          });
        break;
      case '/way/chat':
        this.router.navigate(['way/chat'])
          .then(function (safeAndSound) {
            console.log(safeAndSound);
          });
        break;
      case '/way/settings':
        this.router.navigate(['way/settings'])
          .then(function (safeAndSound) {
            console.log(safeAndSound);
          });
        break;
      case '/way/notifications':
        this.router.navigate(['way/notifications'])
          .then(function (safeAndSound) {
            console.log(safeAndSound);
          });
        break;
      default:
        this.router.navigate(['way/chat'])
          .then(function (safeAndSound) {
            console.log(safeAndSound);
          });
    }
  }

}
