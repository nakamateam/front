import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../_services';
import {NavigationEnd, Router} from '@angular/router';
import {User} from '../../_models';
import {Subscription} from 'rxjs';
import {SettingsService} from '../../_services/settings.service';
import {environment} from '../../../environments/environment';
import {GroupService} from '../../_services/group.service';

@Component({
  selector: 'app-app-navbar',
  templateUrl: './app-navbar.component.html',
  styleUrls: ['./app-navbar.component.scss']
})
export class AppNavbarComponent implements OnInit, OnDestroy {
  forgotpwSelected = false;
  settingsSelected = false;
  navBarBackGround = false;
  isLoginPage = false;
  nakama: User;
  nakamAvatarService: Subscription;
  updateNakamAvatarService: Subscription;
  updateNakamGroupService: Subscription;
  dataLoaded = false;
  avatarUrl = environment.serverHttp + '/avatar/';
  currentGroupName = null;

  constructor(private router: Router,
              public authenticationService: AuthService,
              private settingService: SettingsService,
              private groupService: GroupService) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.forgotpwSelected = this.router.url === '/forgotpw';
        this.settingsSelected = this.router.url === '/settings';
        this.isLoginPage = this.router.url === '/';
        this.navBarBackGround = this.authenticationService.isAuthenticated() === null;
      }
    });
  }

  ngOnInit() {
    this.updateAvatarMenuImage();
    this.updateGroupTitle();
  }

  isAuthenticated() {
    return this.authenticationService.isAuthenticated();
  }

  updateAvatarMenuImage() {
    this.updateNakamAvatarService = this.settingService.currentNakama.subscribe((nakama) => {
      if (nakama.avatarUrl) {
        this.dataLoaded = true;
        this.nakama = nakama;
      } else if (this.authenticationService.isAuthenticated()) {
        this.nakamAvatarService = this.settingService.getNakamaDatas()
          .subscribe(
            remoteNakama => {
              this.dataLoaded = true;
              this.nakama = remoteNakama;
            },
            error => {
              console.log(error);
            });
      } else {
        this.nakama = nakama;
        this.dataLoaded = true;
      }
    });
  }

  updateGroupTitle() {
    this.updateNakamGroupService = this.groupService.currentGroupName.subscribe((wayTitle) => {
      this.currentGroupName = wayTitle;
    }, error => {
    });
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['']);
  }

  isNakamadmin() {
    return this.authenticationService.isAdmin();
  }

  ngOnDestroy(): void {
    if (this.nakamAvatarService) {
      this.nakamAvatarService.unsubscribe();
    }
    if (this.updateNakamAvatarService) {
      this.updateNakamAvatarService.unsubscribe();
    }
    if (this.updateNakamGroupService) {
      this.updateNakamGroupService.unsubscribe();
    }
  }

}
