import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NakamaSettingsComponent } from './nakama-settings.component';

describe('NakamaSettingsComponent', () => {
  let component: NakamaSettingsComponent;
  let fixture: ComponentFixture<NakamaSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NakamaSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NakamaSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
