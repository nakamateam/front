import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../_models';
import {Subscription} from 'rxjs';
import {SettingsService} from '../../_services/settings.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {MatDialog} from '@angular/material';
import {ChangeAvatarComponent} from './change-avatar/change-avatar.component';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-nakama-settings',
  templateUrl: './nakama-settings.component.html',
  styleUrls: ['./nakama-settings.component.scss']
})
export class NakamaSettingsComponent implements OnInit, OnDestroy {

  nakama: User;

  newNakaNameForm = new FormGroup({
    newNakamaUsername: new FormControl('', [
      Validators.required,
      Validators.maxLength(21),
      Validators.minLength(2),
    ]),
    currentPwForUsername: new FormControl('', [
      Validators.required,
    ]),
  });

  emailPattern = new RegExp('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{1,}[.]{1}[a-zA-Z]{1,}');
  newNakaEmailForm = new FormGroup({
    newNakamaEmail: new FormControl('', [
      Validators.required,
      Validators.pattern(this.emailPattern)
    ]),
    currentPwForEmail: new FormControl('', [
      Validators.required,
    ]),
  });

  pwPattern = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{7,}$');
  newNakaPwForm = new FormGroup({
    currentNakamaPw: new FormControl('', [
      Validators.required,
    ]),
    newPw: new FormControl('', [
      Validators.required,
      Validators.pattern(this.pwPattern),
    ]),
    confirmNewPw: new FormControl({value: '', disabled: true}, [Validators.compose(
      [Validators.required, this.validateAreEqual.bind(this)]
    )])
  });

  settingsService: Subscription;
  updateNakamaService: Subscription;
  dataLoaded = false;
  errorMessage = '';
  showSettingErrorMsg = false;
  changeNakaname = false;
  changEmail = false;
  changePw = false;
  avatar: any;
  avatarUrl = environment.serverHttp + '/avatar/';

  constructor(private settingService: SettingsService,
              private toastr: ToastrService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.getNakama();
    document.body.classList.add('settinGif');
  }

  private validateAreEqual(fieldControl: FormControl) {
    return fieldControl.value === this.newNakaPwForm.get('newPw').value ? null : {
      NotEqual: true
    };
  }

  getAvatar() {
    this.settingService.getAvatar().subscribe(file => {
        const reader = new FileReader();
        reader.addEventListener('load', () => {
          this.avatar = reader.result;
        }, false);

        if (file) {
          reader.readAsDataURL(file);
        }
    },
    error => {
      console.log(error);
    });
  }

  getNakama() {
    this.updateNakamaService = this.settingService.getNakamaDatas()
      .subscribe(
        nakama => {
          this.dataLoaded = true;
          this.nakama = nakama;
          this.settingService.changeNakama(nakama);
        },
        error => {
        });
  }

  saveNewNakaName(newNakaNameForm) {
    this.settingsService = this.settingService.updateNakaName(newNakaNameForm)
      .subscribe(
        user => {
          this.showNewNakamaNameSuccess();
          if (this.showSettingErrorMsg) {
            this.showSettingErrorMsg = false;
          }
          this.nakama.username = user.username;
          this.changeNakaname = false;
        }, error => {
          if (error.status === 412) {
            this.errorMessage = error.error.message;
            this.showSettingErrorMsg = true;
          } else {
            this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
            this.showSettingErrorMsg = true;
          }
        }
      );
  }

  saveNewNakaMail(newNakaMailForm) {
    this.settingsService = this.settingService.updateNakaMail(newNakaMailForm)
      .subscribe(
        user => {
          this.showNewNakaMailSuccess();
          if (this.showSettingErrorMsg) {
            this.showSettingErrorMsg = false;
          }
          this.nakama.email = user.email;
          this.changEmail = false;
        }, error => {
          if (error.status === 412) {
            this.errorMessage = error.error.message;
            this.showSettingErrorMsg = true;
          } else {
            this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
            this.showSettingErrorMsg = true;
          }
        }
      );
  }

  saveNewNakaPw(newNakaPwForm) {
    this.settingsService = this.settingService.updateNakaPw(newNakaPwForm)
      .subscribe(
        user => {
          this.showNewNakaPwSuccess();
          if (this.showSettingErrorMsg) {
            this.showSettingErrorMsg = false;
          }
          this.changePw = false;
        }, error => {
          if (error.status === 412) {
            this.errorMessage = error.error.message;
            this.showSettingErrorMsg = true;
          } else {
            this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
            this.showSettingErrorMsg = true;
          }
        }
      );
  }

  // Notification to confirm user a mail has been sent
  showNewNakamaNameSuccess() {
    this.toastr.success('Your username has been changed', '', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 4000,
    });
  }

  showAvatarUpdateSuccess(action) {
    this.toastr.success(action, '', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 4000,
    });
  }

  showNewNakaMailSuccess() {
    this.toastr.success('Your email has been changed successfully', '', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 4000,
    });
  }

  showNewNakaPwSuccess() {
    this.toastr.success('Your password has been changed successfully', '', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 4000,
    });
  }

  updateNakamaName() {
    this.changeNakaname = !this.changeNakaname;
  }

  updateNakaMail() {
    this.changEmail = !this.changEmail;
  }

  updatePw() {
    this.changePw = !this.changePw;
  }

  resetBoolSettings() {
    this.changeNakaname = false;
    this.changEmail = false;
    this.changePw = false;
    this.showSettingErrorMsg = false;
  }

  actionsNeeded() {
    return (!this.changeNakaname && !this.changEmail && !this.changePw);
  }

  checkPass(): string {
    const regex = new Array();
    regex.push('[A-Z]'); // Uppercase Alphabet.
    regex.push('[a-z]'); // Lowercase Alphabet.
    regex.push('[0-9]'); // Digit.
    let passed = 0;
    // Validate for each Regular Expression.
    for (let i = 0; i < regex.length; i++) {
      if (new RegExp(regex[0]).test(this.newNakaPwForm.get('newPw').value)) {
        passed++;
      } else {
        this.newNakaPwForm.get('confirmNewPw').disable();
        return 'uppercase';
      }
      if (new RegExp(regex[1]).test(this.newNakaPwForm.get('newPw').value)) {
        passed++;
      } else {
        this.newNakaPwForm.get('confirmNewPw').disable();
        return 'Lowercase';
      }
      if (new RegExp(regex[2]).test(this.newNakaPwForm.get('newPw').value)) {
        passed++;
      } else {
        this.newNakaPwForm.get('confirmNewPw').disable();
        return 'Digit';
      }
      if (this.newNakaPwForm.get('newPw').value.length >= 7) {
        passed++;
      } else {
        this.newNakaPwForm.get('confirmNewPw').disable();
        return 'atleast7';
      }
      if (passed === 4) {
        this.newNakaPwForm.get('confirmNewPw').enable();
        return 'clean';
      }
    }
  }

  openAvatarDialog(): void {
    const dialogRef = this.dialog.open(ChangeAvatarComponent, {
      width: '550px',
      autoFocus: false,
      data: {nakama: this.nakama}
    });
    dialogRef.afterClosed().subscribe(action => {
      if (action) {
        this.showAvatarUpdateSuccess(action);
      }
      this.getNakama();
    });
  }

  ngOnDestroy(): void {
    if (this.settingsService) {
      this.settingsService.unsubscribe();
    }
    if (this.updateNakamaService) {
      this.updateNakamaService.unsubscribe();
    }
    document.body.classList.remove('settinGif');
  }

}
