import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FileItem, FileUploader, ParsedResponseHeaders} from 'ng2-file-upload';
import {WebcamImage} from 'ngx-webcam';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {SettingsService} from '../../../_services/settings.service';


@Component({
  selector: 'app-change-avatar',
  templateUrl: './change-avatar.component.html',
  styleUrls: ['./change-avatar.component.scss']
})
export class ChangeAvatarComponent implements OnInit {

  showWebcamStream = true;
  private trigger: Subject<void> = new Subject<void>();
  public webcamImage: WebcamImage = null;
  pictureTaken = false;
  file: File[] = [];
  croppedImage: string = null;
  takePictureButtons = false;
  imageCrop = false;
  imageChangedEvent: any = '';
  avatarUrl = environment.serverHttp + '/upload';
  private keyUser = 'nakamaUser';
  currentUser = JSON.parse(localStorage.getItem(this.keyUser));
  public uploader: FileUploader = new FileUploader({
    url: this.avatarUrl,
    itemAlias: 'avatar',
    headers: [{name: 'x-xsrf-token', value: this.currentUser.token}]
  });
  confirmButtonDisabled = true;
  badGifDimension = false;
  gifDetected = false;
  badAvatarFormat = false;
  fileTooBigMessage = '';
  avatarSizeTooBig = false;


  constructor(public dialogRef: MatDialogRef<ChangeAvatarComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private settingService: SettingsService) {
    this.uploader.onCompleteAll = () => {
            this.dialogRef.close('Your avatar image has been changed successfully');
    };
  }

  ngOnInit() {
    this.uploader.onBeforeUploadItem = (item: FileItem) => {
      this.uploader.options.additionalParameter = {
        nakId: this.data.nakama._id,
      };
    };
    this.uploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
    this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    // console.log(response);
  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    // console.log(response);
  }

  /** FONCTIONS CONCERNANT LA WEBCAM */
  public triggerSnapshot(): void {
    this.trigger.next();
    this.toggleWebcam();
  }

  triggerNewSnapshot() {
    this.webcamImage = null;
    this.triggerSnapshot();
    this.confirmButtonDisabled = true;
  }

  public toggleWebcam(): void {
    this.showWebcamStream = !this.showWebcamStream;
    this.pictureTaken = !this.pictureTaken;
  }

  activateCamera() {
    this.gifDetected = false;
    this.badGifDimension = false;
    this.badAvatarFormat = false;
    this.showWebcamStream = true;
    this.croppedImage = null;
    this.takePictureButtons = true;
    this.imageCrop = false;
    this.enableUploadButton();
  }

  public handleImage(webcamImage: WebcamImage): void {
    this.webcamImage = webcamImage;
    this.enableUploadButton();
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  /** FONCTIONS CONCERNANT L'INPUT IMAGE ET LE CROP */
  resetWebcamSettings(): void {
    this.webcamImage = null;
    this.pictureTaken = false;
    this.showWebcamStream = false;
    this.takePictureButtons = false;
  }

  badFormat(file) {
    switch (file) {
      case 'image/gif': {
        return false;
      }
      case 'image/png': {
        return false;
      }
      case 'image/jpeg': {
        return false;
      }
      default: {
        return true;
      }
    }
  }

  fileChangeEvent(event: any): void {
    this.badGifDimension = false;
    this.gifDetected = false;
    this.badAvatarFormat = false;
    this.avatarSizeTooBig = false;
    const changeAvatar = this;
    this.imageCrop = true;
    this.imageChangedEvent = event;
    // Gif Handler
    if (event.target.files.length > 0) {
      if (this.badFormat(event.target.files[0].type)) {
        this.badAvatarFormat = true;
        this.imageCrop = false;
      } else if (event.target.files[0].size > 10 * 1024 * 1024) {
        this.fileTooBigMessage = 'Maximum upload size exceeded (' + Math.round( event.target.files[0].size / 1048576 * 10 ) / 10 + ' Mb of 10Mb allowed)';
        this.avatarSizeTooBig = true;
        this.imageCrop = false;
      }
      if (event.target.files[0].type === 'image/gif') {
        this.gifDetected = true;
        // determine gif height and width
        const _URL = window.URL;
        let file, img;
        if ((file = event.target.files[0])) {
          img = new Image();
          img.onload = function () {
            if (this.width === this.height) {
              changeAvatar.addGifTQueue(file);
              changeAvatar.badGifDimension = false;
            } else {
              changeAvatar.badGifDimension = true;
            }
          };
          img.src = _URL.createObjectURL(file);
        }
      }
    }
    // Image Handler
    if (event.target.files.length === 0) {
      this.imageCrop = false;
      this.croppedImage = null;
      this.webcamImage = null;
      this.enableUploadButton();
    }
  }

  /** Outputs image in base64 */
  imageCropped(image: string) {
    if (this.gifDetected) {
      if (this.badGifDimension) {
        this.croppedImage = null;
      }
    } else {
      this.croppedImage = image;
      this.addToQueue(this.croppedImage);
    }
    this.enableUploadButton();
  }

  /** Créer un objet File à partir de l'image en base64 */
  dataURLtoFile(dataurl, filename) {
    const arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type: mime});
  }

  /** FONCTION CHARGEE DE L'UPLOAD DE L'IMAGE */
  onUpload() {
    if (this.uploader.queue.length > 0) {
        this.uploader.uploadAll();
    }
  }

  deleteNakamAvatar() {
    this.settingService.removeAvatar().subscribe(res => {
        this.dialogRef.close('Your avatar image has been deleted successfully');
      },
      error => {
      });
  }

  /** Activate the confirm button if the image object is not empty otherwise the button is deactivated */
  enableUploadButton() {
    if (this.croppedImage != null || this.webcamImage != null || (this.gifDetected && !this.badGifDimension)) {
      this.confirmButtonDisabled = false;
    } else {
      this.confirmButtonDisabled = true;
    }
  }

  /** Adding cropped, snapchot image or square gif to queue for upload */
  addToQueue(dataUrl) {
    this.uploader.clearQueue();
    this.file = [];
    const file = this.dataURLtoFile(dataUrl, this.data.nakama._id);
    this.file.push(file);
    this.uploader.addToQueue(this.file);
  }

  addGifTQueue(gif) {
    this.uploader.clearQueue();
    this.file = [];
    this.file.push(gif);
    this.uploader.addToQueue(this.file);
    this.uploader.queue[0].file.name = this.data.nakama._id;
    this.enableUploadButton();
  }

  /** close ChangeAvatar Dialog */
  onNoClick(): void {
    this.dialogRef.close();
  }

}
