import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../_services';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../_models';
import {Subscription} from 'rxjs';
import {MatDialog} from '@angular/material';
import {AboutUsComponent} from './about-us/about-us.component';
import {SettingsService} from '../../_services/settings.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public showLoginErrorMsg = false;
  errorMessage = '';
  constructor(private authenticationService: AuthService,
              private router: Router,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              private settingService: SettingsService) {}

  loginService: Subscription;

  newNakama = new User();
  returnUrl: string;

  ngOnInit() {
    document.body.classList.add('nakamaLoGif');
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
  }

  onSubmit() {
    this.loginService = this.authenticationService.login(this.newNakama.username, this.newNakama.password)
      .subscribe(
        data => {
          const nakamHasAvatar = new User();
          nakamHasAvatar.avatarUrl = data.avatarUrl;
          this.settingService.changeNakama(nakamHasAvatar);
          this.router.navigate([this.returnUrl]);
        }, error => {
          if (error.status === 412) {
            this.errorMessage = error.error.message;
            this.showLoginErrorMsg = true;
          } else {
            console.log(error);
            this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
            this.showLoginErrorMsg = true;
          }
        }
      );
  }

  reInitError() {
    console.log('zouwoups');
      this.showLoginErrorMsg = false;
  }

  ngOnDestroy() {
    if (this.loginService) {
      this.loginService.unsubscribe();
    }
    this.showLoginErrorMsg = false;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AboutUsComponent, {
      width: '650px',
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
