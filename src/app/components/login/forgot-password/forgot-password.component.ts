import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../../_services';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return (control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-reset-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {

  emailPattern = new RegExp('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{1,}[.]{1}[a-zA-Z]{1,}');
  nakamail = new FormControl('', [
    Validators.required,
    Validators.pattern(this.emailPattern),
  ]);
  matcher = new MyErrorStateMatcher();
  pwForgotService: Subscription;
  showEmailErrorMsg = false;
  errorMessage = '';

  constructor(public authService: AuthService,
              private route: ActivatedRoute,
              private router: Router,
              private toastr: ToastrService) { }

  ngOnInit() {
    document.body.classList.add('nakamaLoGif');
  }

  onSubmit() {
    this.pwForgotService = this.authService.sendMeEmailToResetMyPw(this.nakamail.value)
      .subscribe(
        data => {
          this.router.navigate(['/']);
          this.showSuccess();
        }, error => {
          if (error.status === 412) {
            this.errorMessage = error.error.message;
            this.showEmailErrorMsg = true;
          } else {
            this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
            this.showEmailErrorMsg = true;
          }
        }
      );
  }

  reInitError() {
    this.showEmailErrorMsg = false;
  }

  // Notification to confirm user a mail has been sent
  showSuccess() {
    this.toastr.success('An email has just been sent to ' + this.nakamail.value, '', {
      closeButton: true,
      positionClass: 'toast-bottom-center',
      timeOut: 4000,
    });
  }

  ngOnDestroy(): void {
    document.body.classList.remove('nakamaLoGif');
    if (this.pwForgotService) {
      this.pwForgotService.unsubscribe();
    }
    this.showEmailErrorMsg = false;
  }

}
