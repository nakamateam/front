import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {AuthService} from '../../../_services';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  newPassword = '';
  confirmPassword = '';
  access: Boolean = false;
  randomcolor = 'red';
  resetPwService: Subscription;
  reseToken = '';
  showResetPwErrorMsg = false;
  errorMessage = '';

  constructor(public authService: AuthService,
              private route: ActivatedRoute,
              private router: Router,
              private toastr: ToastrService) {
    this.route.queryParams.subscribe(params => {
      this.reseToken = params['token'];
    });
  }

  ngOnInit() {
    if (!this.reseToken) {
      this.router.navigate(['/']);
    }
  }

  onSubmit() {
    this.resetPwService = this.authService.resetMyPw(this.reseToken, this.newPassword, this.confirmPassword)
      .subscribe(
        data => {
          this.router.navigate(['/']);
          this.showSuccess();
        }, error => {
          if (error.status === 412) {
            this.errorMessage = error.error.message;
            this.showResetPwErrorMsg = true;
          } else {
            this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
            this.showResetPwErrorMsg = true;
          }
        }
      );
  }

  reInitError() {
    this.showResetPwErrorMsg = false;
  }

  // Notification to confirm user a mail has been sent
  showSuccess() {
    this.toastr.success('Your password has been reset.', '', {
      closeButton: true,
      positionClass: 'toast-bottom-center',
      timeOut: 4000,
    });
  }

  checkPass(): boolean {
    let condPass = 'Le mot de passe doit contenir :<br>';
    const regex = new Array();
    regex.push('[A-Z]'); // Uppercase Alphabet.
    regex.push('[a-z]'); // Lowercase Alphabet.
    regex.push('[0-9]'); // Digit.
    let passed = 0;
    // Validate for each Regular Expression.
    for (let i = 0; i < regex.length; i++) {
      if (new RegExp(regex[i]).test(this.newPassword)) {
        passed++;
      } else {
        switch (i) {
          case 0: {
            condPass += '- Des lettres majuscules<br>';
            break;
          }
          case 1: {
            condPass += '- Des lettres miniscules<br>';
            break;
          }
          case 2: {
            condPass += '- Des chiffres<br>';
            break;
          }
        }
      }
    }
    // Validate for length of Password.
    if (passed > 1 && this.newPassword.length >= 7) {
      passed++;
    } else {
      condPass += '- Une longueur minimale de 7 caractères<br>';
    }
    // Display status.
    let color = 'red';

    if (passed < 4) {
      color = 'red';
    } else if (passed === 4) {
      condPass = 'Mot de passe conforme';
      color = 'darkgreen';
      this.access = true;
    }

    if (this.newPassword.length === 0) {
      this.access = false;
    }

    this.randomcolor = color;
    document.getElementById('password_strength').innerHTML = condPass;

      return condPass === 'Mot de passe conforme';
  }

}
