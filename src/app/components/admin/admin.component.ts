import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {AdminService} from '../../_services/admin.service';
import {Group} from '../../_models';
import {faDatabase} from '@fortawesome/free-solid-svg-icons';
import {MatDialog, Sort} from '@angular/material';
import {HomeComponent} from '../home/home.component';
import {NakamaDetailDialogComponent} from '../../nakama-detail-dialog/nakama-detail-dialog.component';
import {AddMemberComponent} from './add-member/add-member.component';
import {ManageUserComponent} from './manage-user/manage-user.component';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit, OnDestroy {

  getnakamaWayGroupsService: Subscription;
  getMongoDbStatsService: Subscription;
  getTotalGroupMembers: Observable<number> = this.adminService.getNakamaWayTotalUsers();
  nakamaWayGroups: Group[];
  sortedNakamaWayGroups: Group[];
  dbStats: any;
  dataLoaded = false;
  dbStatsLoaded = false;
  totalSize = 0;
  faDb = faDatabase;
  sortInit: Sort = { active: 'creation', direction: 'desc' };
  errorMessage = '';
  showSettingErrorMsg = false;


  constructor(private adminService: AdminService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.getnakamaWayGroups();
    this.getMongoDbStats();
  }

  getnakamaWayGroups() {
    this.getnakamaWayGroupsService = this.adminService.getWayDashboard()
      .subscribe(
        dashboard => {
          console.log(dashboard);
          this.dataLoaded = true;
          this.nakamaWayGroups = dashboard;
          this.sortedNakamaWayGroups = this.nakamaWayGroups.slice();
          this.sortData(this.sortInit);
          this.sumTotalShareFileSpace(dashboard);
        },
        error => {
          if (error.status === 401) {
            this.errorMessage = 'Not sure you are an admin user...';
            this.showSettingErrorMsg = true;
          } else {
            this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
            this.showSettingErrorMsg = true;
          }
        });
  }

  getMongoDbStats() {
    this.getMongoDbStatsService = this.adminService.getMongoDbStats()
      .subscribe(
        dbStats => {
          console.log(dbStats);
          this.dbStats = dbStats;
          this.dbStatsLoaded = true;
        },
        error => {
          if (error.status === 401) {
            this.errorMessage = 'Not sure you are an admin user...';
            this.showSettingErrorMsg = true;
          } else {
            this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
            this.showSettingErrorMsg = true;
          }
        });
  }


  sumTotalShareFileSpace(dashboard: Group[]): number {
    dashboard.forEach(group => {
      this.totalSize = this.totalSize + group.groupSize;
    });
    return this.totalSize;
  }

  sortData(sort: Sort) {
    const data = this.nakamaWayGroups.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedNakamaWayGroups = data;
      return;
    }

    this.sortedNakamaWayGroups = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return HomeComponent.compare(a.groupName, b.groupName, isAsc);
        case 'creation': return HomeComponent.compare(a.creationDate.toString(), b.creationDate.toString(), isAsc);
        case 'members': return HomeComponent.compare(a.users.length, b.users.length, isAsc);
        case 'size': return HomeComponent.compare(a.groupSize, b.groupSize, isAsc);
        default: return 0;
      }
    });
  }

  openNakamaDetailDialog(avatarUrl): void {
    const dialogRef = this.dialog.open(NakamaDetailDialogComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {avatarUrl: avatarUrl}
    });
    dialogRef.afterClosed().subscribe(confirm => {
    });
  }

  openManageUserDialog(): void {
    const dialogRef = this.dialog.open(ManageUserComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
    });
    dialogRef.afterClosed().subscribe(confirm => {
    });
  }

  openNakamaCreationDialog(): void {
    const dialogRef = this.dialog.open(AddMemberComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
    });
    dialogRef.afterClosed().subscribe(nakama => {
      if (nakama && nakama.newUser) {
        this.getTotalGroupMembers = this.adminService.getNakamaWayTotalUsers();
      }
    });
  }

  ngOnDestroy(): void {
    this.getnakamaWayGroupsService.unsubscribe();
    this.getMongoDbStatsService.unsubscribe();
  }

}
