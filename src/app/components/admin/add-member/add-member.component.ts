import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserRole} from '../../../_models';
import {Subscription} from 'rxjs';
import {AdminService} from '../../../_services/admin.service';
import {ToastrService} from 'ngx-toastr';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.scss']
})
export class AddMemberComponent implements OnInit, OnDestroy {
  emailPattern = new RegExp('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{1,}[.]{1}[a-zA-Z]{1,}');
  pwPattern = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{7,}$');
  rolePattern = new RegExp(`^(${UserRole[UserRole.NAKAMADMIN]}|${UserRole[UserRole.NAKAMA]})`);
  userRole = UserRole;
  createNakamaService: Subscription;
  blockNakamaService: Subscription;
  errorMessage = '';
  showSettingErrorMsg = false;

  newNakamaForm = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.maxLength(21),
      Validators.minLength(2),
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.pattern(this.emailPattern)
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern(this.pwPattern),
    ]),
    role: new FormControl('', [
      Validators.required,
      Validators.pattern(this.rolePattern),
    ]),
    firstName: new FormControl('', [
      Validators.required,
      Validators.maxLength(21),
      Validators.minLength(2),
    ]),
    lastName: new FormControl('', [
      Validators.required,
      Validators.maxLength(21),
      Validators.minLength(2),
    ]),
  });

  blockNakamaForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern(this.emailPattern)
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern(this.pwPattern),
    ]),
  });
  constructor(private adminService: AdminService,
              private toastr: ToastrService,
              public dialogRef: MatDialogRef<AddMemberComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

  saveNewNakama(newNakaPwForm) {
    this.createNakamaService = this.adminService.createNewnakama(newNakaPwForm)
      .subscribe(
        user => {
          console.log(user);
          this.showNewNakamaCreatedSuccess(user.username);
          this.dialogRef.close({newUser: true});
        }, error => {
          if (error.status === 401) {
            this.errorMessage = 'Not sure you are an admin user...';
            this.showSettingErrorMsg = true;
          } else {
            this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
            this.showSettingErrorMsg = true;
          }
        }
      );
  }

  blockNakama(blockNakamaForm) {
    if (this.data) {
      blockNakamaForm._id = this.data.user._id;
      this.createNakamaService = this.adminService.blockMember(blockNakamaForm)
        .subscribe(
          user => {
            console.log(user);
            this.showNakamaBlockedSuccess(user.username);
            this.dialogRef.close();
          }, error => {
            if (error.status === 401) {
              this.errorMessage = 'Not sure you are an admin user...';
              this.showSettingErrorMsg = true;
            } else {
              this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
              this.showSettingErrorMsg = true;
            }
          }
        );
    }
  }

  // Notification to confirm user has been created
  showNewNakamaCreatedSuccess(user) {
    this.toastr.success(user + ' has been created successfully as a new nakama member', '', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 4000,
    });
  }

  showNakamaBlockedSuccess(user) {
    this.toastr.success(user + ' has been successfully blocked', '', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 4000,
    });
  }

  checkPass(): string {
    const regex = new Array();
    regex.push('[A-Z]'); // Uppercase Alphabet.
    regex.push('[a-z]'); // Lowercase Alphabet.
    regex.push('[0-9]'); // Digit.
    let passed = 0;
    // Validate for each Regular Expression.
    for (let i = 0; i < regex.length; i++) {
      if (new RegExp(regex[0]).test(this.newNakamaForm.get('password').value)) {
        passed++;
      } else {
        return 'uppercase';
      }
      if (new RegExp(regex[1]).test(this.newNakamaForm.get('password').value)) {
        passed++;
      } else {
        return 'Lowercase';
      }
      if (new RegExp(regex[2]).test(this.newNakamaForm.get('password').value)) {
        passed++;
      } else {
        return 'Digit';
      }
      if (this.newNakamaForm.get('password').value.length >= 7) {
        passed++;
      } else {
        return 'atleast7';
      }
      if (passed === 4) {
        return 'clean';
      }
    }
  }

  ngOnDestroy(): void {
    if (this.createNakamaService) {
      this.createNakamaService.unsubscribe();
    }
    if (this.blockNakamaService) {
      this.blockNakamaService.unsubscribe();
    }
  }

}
