import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AdminService} from '../../../_services/admin.service';
import {User} from '../../../_models';
import {MatDialog, Sort} from '@angular/material';
import {HomeComponent} from '../../home/home.component';
import {AddMemberComponent} from '../add-member/add-member.component';

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.scss']
})
export class ManageUserComponent implements OnInit, OnDestroy {
  getMembersService: Subscription;
  members: User[];
  sortedMembers: User[];
  errorMessage = '';
  showSettingErrorMsg = false;
  sortInit: Sort = { active: 'username', direction: 'asc' };
  dataLoaded = false;

  constructor(private adminService: AdminService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.getAllMembers();
  }

  getAllMembers() {
    this.getMembersService = this.adminService.getAllMembers()
      .subscribe(
        users => {
          console.log(users);
          this.members = users;
          this.sortedMembers = this.members.slice();
          this.sortData(this.sortInit);
          this.dataLoaded = true;
        }, error => {
          if (error.status === 401) {
            this.errorMessage = 'Not sure you are an admin user...';
            this.showSettingErrorMsg = true;
          } else {
            this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
            this.showSettingErrorMsg = true;
          }
        }
      );
  }

  sortData(sort: Sort) {
    const data = this.members.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedMembers = data;
      return;
    }

    this.sortedMembers = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'username': return HomeComponent.compare(a.username, b.username, isAsc);
        default: return 0;
      }
    });
  }

  openNakamaCreationDialog(user: User): void {
    const dialogRef = this.dialog.open(AddMemberComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {user: user}
    });
    dialogRef.afterClosed().subscribe(nakama => {
    });
  }

  ngOnDestroy(): void {
    this.getMembersService.unsubscribe();
  }

}
