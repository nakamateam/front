import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NakamaGroupComponent } from './nakama-group.component';

describe('NakamaGroupComponent', () => {
  let component: NakamaGroupComponent;
  let fixture: ComponentFixture<NakamaGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NakamaGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NakamaGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
