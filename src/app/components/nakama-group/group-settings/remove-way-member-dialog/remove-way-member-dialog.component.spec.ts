import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveWayMemberDialogComponent } from './remove-way-member-dialog.component';

describe('RemoveWayMemberDialogComponent', () => {
  let component: RemoveWayMemberDialogComponent;
  let fixture: ComponentFixture<RemoveWayMemberDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveWayMemberDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveWayMemberDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
