import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {GroupService} from '../../../../_services/group.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-remove-way-member-dialog',
  templateUrl: './remove-way-member-dialog.component.html',
  styleUrls: ['./remove-way-member-dialog.component.scss']
})
export class RemoveWayMemberDialogComponent implements OnInit, OnDestroy {

  removeFromGroupService: Subscription;

  constructor(public dialogRef: MatDialogRef<RemoveWayMemberDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private groupService: GroupService) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  removeFromGroup() {
    this.removeFromGroupService = this.groupService.removeFromGroup(this.data.memberId, this.data.wayId)
      .subscribe(
        wayRole => {
          this.dialogRef.close({deleteConfirmation: true});
        }, error => {
          this.dialogRef.close({deleteConfirmation: false, error: error});
        }
      );
  }

  ngOnDestroy(): void {
    if (this.removeFromGroupService) {
      this.removeFromGroupService.unsubscribe();
    }
  }

}
