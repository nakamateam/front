import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FileItem, FileUploader, ParsedResponseHeaders} from 'ng2-file-upload';
import {environment} from '../../../../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import {GroupService} from '../../../../_services/group.service';

@Component({
  selector: 'app-change-group-avatar-dialog',
  templateUrl: './change-group-avatar-dialog.component.html',
  styleUrls: ['./change-group-avatar-dialog.component.scss']
})
export class ChangeGroupAvatarDialogComponent implements OnInit {

  imageCrop = false;
  imageChangedEvent: any = '';
  badGifDimension = false;
  gifDetected = false;
  badAvatarFormat = false;
  fileTooBigMessage = '';
  avatarSizeTooBig = false;
  requestErrorMessage = '';
  requestError = false;
  confirmButtonDisabled = true;
  croppedImage: string = null;
  private keyUser = 'nakamaUser';
  currentUser = JSON.parse(localStorage.getItem(this.keyUser));
  groupAvatarUrl = environment.serverHttp + '/group-avatar';
  public uploader: FileUploader = new FileUploader({
    url: this.groupAvatarUrl,
    itemAlias: 'way-avatar',
    headers: [{name: 'x-xsrf-token', value: this.currentUser.token}]
  });
  file: File[] = [];
  @ViewChild('fileInput') fileInput: ElementRef;


  constructor(public dialogRef: MatDialogRef<ChangeGroupAvatarDialogComponent>,
              private toastr: ToastrService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private groupService: GroupService) { }

  ngOnInit() {
    this.initUploader();
    if (!this.data.wayInfo.hasAvatar) {
      this.fileInput.nativeElement.click();
    }
  }

  initUploader() {
    this.uploader.onBeforeUploadItem = (item: FileItem) => {
      const wayIdHeader = this.uploader.options.headers.find(function (element) {
        return element.name === 'way-id';
      });
      if (!wayIdHeader) {
        const wayId = {name: 'way-id', value: this.data.wayInfo._id};
        this.uploader.options.headers.push(wayId);
      }
      this.uploader.options.additionalParameter = {
        wayId: this.data.wayInfo._id,
      };
    };
    this.uploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
    this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    console.log('onSuccessItem');
    this.showAvatarUpdateSuccess();
    this.dialogRef.close({hasAvatar: true, avatarUrl: response});
  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    console.log(status);
    console.log(response);
    this.showAvatarUploadError();
    if (status === 401) {
      this.dialogRef.close({error: true, status: status});
    } else if (status === 405) {
      this.requestErrorMessage = response;
      this.requestError = true;
    } else if (status === 406) {
      this.requestErrorMessage = response;
      this.requestError = true;
    }
  }

  showAvatarUpdateSuccess() {
    this.toastr.success(this.data.wayInfo.groupName + ' avatar has been changed successfully', '', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 4000,
    });
  }

  showAvatarUploadError() {
    this.toastr.error(  this.data.wayInfo.groupName + '\'s avatar has not been changed.' +
      ' If the problem persists, contact your administrator.', 'Way Avatar upload Error', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 8000,
    });
  }

  /** close ChangeAvatar Dialog */
  onNoClick(): void {
    this.dialogRef.close();
  }

  fileChangeEvent(event: any): void {
    this.badGifDimension = false;
    this.gifDetected = false;
    this.requestError = false;
    this.badAvatarFormat = false;
    this.avatarSizeTooBig = false;
    const changeAvatar = this;
    this.imageCrop = true;
    this.croppedImage = null;
    this.imageChangedEvent = event;
    // Gif Handler
    if (event.target.files.length > 0) {
      if (this.badFormat(event.target.files[0].type)) {
        this.badAvatarFormat = true;
        this.imageCrop = false;
        this.enableUploadButton();
      } else if (event.target.files[0].size > 10 * 1024 * 1024) {
        this.fileTooBigMessage = 'Maximum upload size exceeded (' + Math.round( event.target.files[0].size / 1048576 * 10 ) / 10 +
          ' Mb of 10Mb allowed)';
        this.avatarSizeTooBig = true;
        this.imageCrop = false;
        this.enableUploadButton();
      }
      if (event.target.files[0].type === 'image/gif') {
        this.gifDetected = true;
        // determine gif height and width
        const _URL = window.URL;
        let file, img;
        if ((file = event.target.files[0])) {
          img = new Image();
          img.onload = function () {
            if (this.width === this.height) {
              changeAvatar.addGifTQueue(file);
              changeAvatar.badGifDimension = false;
              changeAvatar.enableUploadButton();

            } else {
              changeAvatar.badGifDimension = true;
              changeAvatar.enableUploadButton();
            }
          };
          img.src = _URL.createObjectURL(file);
        }
      }
    } else {
      this.imageCrop = false;
      this.croppedImage = null;
      this.enableUploadButton();
    }
  }

  /** Activate the confirm button if the image object is not empty otherwise the button is deactivated */
  enableUploadButton() {
    this.confirmButtonDisabled = !(this.croppedImage !== null || (this.gifDetected && !this.badGifDimension));
  }

  addGifTQueue(gif) {
    this.uploader.clearQueue();
    this.file = [];
    this.file.push(gif);
    this.uploader.addToQueue(this.file);
    this.uploader.queue[0].file.name = this.data.wayInfo.groupName;
    this.enableUploadButton();
  }

  badFormat(file) {
    switch (file) {
      case 'image/gif': {
        return false;
      }
      case 'image/png': {
        return false;
      }
      case 'image/jpeg': {
        return false;
      }
      default: {
        return true;
      }
    }
  }

  /** FONCTION CHARGEE DE L'UPLOAD DE L'IMAGE */
  onUpload() {
    if (this.uploader.queue.length > 0) {
      this.uploader.uploadAll();
    }
  }

  deleteWayAvatar() {
    this.groupService.removeGroupAvatar(this.data.wayInfo._id).subscribe(res => {
        this.dialogRef.close({hasAvatar: false, avatarUrl: res});
      },
      error => {
        if (error && error.status === 405) {
          this.requestErrorMessage = error.error;
          this.requestError = true;
        } else if (error && error.status === 406) {
          this.requestErrorMessage = error.error;
          this.requestError = true;
        } else {
          this.requestErrorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
          this.requestError = true;
        }
      });
  }

  /** Créer un objet File à partir de l'image en base64 */
  dataURLtoFile(dataurl, filename) {
    const arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type: mime});
  }

  /** Adding cropped, snapchot image or square gif to queue for upload */
  addToQueue(dataUrl) {
    this.uploader.clearQueue();
    this.file = [];
    const file = this.dataURLtoFile(dataUrl, this.data.wayInfo.groupName);
    this.file.push(file);
    this.uploader.addToQueue(this.file);
  }

  /** Outputs image in base64 */
  imageCropped(image: string) {
    if (this.gifDetected) {
      if (this.badGifDimension) {
        this.croppedImage = null;
      }
    } else {
      this.croppedImage = image;
      this.addToQueue(this.croppedImage);
    }
    this.enableUploadButton();
  }

}
