import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeGroupAvatarDialogComponent } from './change-group-avatar-dialog.component';

describe('ChangeGroupAvatarDialogComponent', () => {
  let component: ChangeGroupAvatarDialogComponent;
  let fixture: ComponentFixture<ChangeGroupAvatarDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeGroupAvatarDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeGroupAvatarDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
