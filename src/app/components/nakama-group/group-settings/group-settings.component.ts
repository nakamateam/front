import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {GroupService} from '../../../_services/group.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NakamaDetailDialogComponent} from '../../../nakama-detail-dialog/nakama-detail-dialog.component';
import {MatDialog} from '@angular/material';
import {debounceTime, distinctUntilChanged, map, startWith, switchMap} from 'rxjs/operators';
import {ChangeGroupAvatarDialogComponent} from './change-group-avatar-dialog/change-group-avatar-dialog.component';
import {groupRole, Group, User} from '../../../_models';
import {RemoveWayMemberDialogComponent} from './remove-way-member-dialog/remove-way-member-dialog.component';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-group-settings',
  templateUrl: './group-settings.component.html',
  styleUrls: ['./group-settings.component.scss']
})
export class GroupSettingsComponent implements OnInit, OnDestroy {
  @Output() eventInBottomSheet: EventEmitter<any> = new EventEmitter<any>();
  @Input() groupDetails: any;
  updateGroupService: Subscription;
  getGroupInfoService: Subscription;
  myCurrentWay: Group;
  isGroupSelected = localStorage.getItem('myWay') !== null;
  dataLoaded = false;
  searchMembers = new FormControl();
  filteredMembers: Observable<any[]>;
  members: any[] = [];
  wayName: string = null;
  hideSaveButton: Boolean = true;
  updateWayNameService: Subscription;
  removeAdminRightsService: Subscription;
  makeAdminService: Subscription;
  showGroupSettingErrorMsg: Boolean = false;
  errorMessage: string;

  getNewMembersService: Subscription;
  switchMemberOptionButon: Boolean = true;
  filteredOptions: Observable<User[]>;
  addMembers = new FormControl();
  rawfilteredOptions: User[];
  addWayMemberService: Subscription;

  newNakaGroupForm = new FormGroup({
    newNakamaGroupName: new FormControl('', [
      Validators.required,
      Validators.maxLength(50),
    ]),
  });

  get groupName() { return this.newNakaGroupForm.get('newNakamaGroupName');  }
  setNakaGroupNameValue(value) { this.newNakaGroupForm.setValue({newNakamaGroupName: value}); }

  constructor(private groupService: GroupService,
              public dialog: MatDialog,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.getGroupInfo();
    this.initMembersFilter();
    if (!this.groupDetails) {
      this.updateGroup();
      this.onNewNakaGroupFormChanges();
    } else {
      this.switchMemberOptionButon = !this.groupDetails.isAdmin;
    }
    this.initAddMemberField();
  }

  /** Used to reload GroupInfo if user go to another way through search bar */
  updateGroup() {
    this.updateGroupService = this.groupService.currentGroup.subscribe((group) => {
        console.log(group);
        this.myCurrentWay = group;
        if (group !== null) {
          this.getGroupInfo();
        }
      },
      error => {
      });
  }

  getGroupInfo() {
    this.dataLoaded = false;
    if (this.isGroupSelected || this.groupDetails) {
      this.getGroupInfoService = this.groupService.getGroupInfo(localStorage.getItem('myWay') || this.groupDetails._id)
        .subscribe((group) => {
            console.log(group);
            this.myCurrentWay = group;
            this.members = group.users;
            this.wayName = group.groupName;
            this.setNakaGroupNameValue(group.groupName);
            this.dataLoaded = true;
          },
          error => {
            this.handleError(error);
          });
    }
  }

  initAddMemberField() {
    this.filteredOptions = this.addMembers.valueChanges
      .pipe(
        startWith(''),
        debounceTime(400),
        distinctUntilChanged(),
        switchMap(val => {
          return this.filter(val || '');
        })
      );

    this.getNewMembersService = this.filteredOptions
      .subscribe(mySearchlist => {
          console.log(mySearchlist);
          this.rawfilteredOptions = mySearchlist;
          if (this.groupDetails) {
            this.eventInBottomSheet.emit(true);
          }
        },
        error => {
        });
  }

  // filter and return the values
  filter(val: string): Observable<User[]> {
    console.log(val);
    return this.groupService.searchNewMember(val, this.myCurrentWay._id)
      .pipe(
        map(response => response.filter(option => {
          if (val.length >= 1) {
            return option;
          }
        }))
      );
  }

  onNewNakaGroupFormChanges(): void {
    this.newNakaGroupForm.get('newNakamaGroupName').valueChanges.subscribe(val => {
        this.hideSaveButton = val === this.wayName;
    });
  }

  initMembersFilter() {
    this.filteredMembers = this.searchMembers.valueChanges
      .pipe(
        startWith(''),
        map(value => this.filterMembers(value))
      );
  }

  private filterMembers(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.members.filter(member => member.username.toLowerCase().includes(filterValue) ||
      member.firstName.toLowerCase().includes(filterValue) ||
      member.lastName.toLowerCase().includes(filterValue));
  }

  displayFn(user?: User): string | undefined {
    return user ? user.username : undefined;
  }

  openNakamaDetailDialog(avatarUrl): void {
    const dialogRef = this.dialog.open(NakamaDetailDialogComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {avatarUrl: avatarUrl}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      // this.fixButtonRippleEffectBug(this.buttonDeleteNakama);
    });
  }

  saveNewWayName() {
    this.updateWayNameService = this.groupService.updateWayName(this.groupName.value, this.myCurrentWay._id)
      .subscribe(
        groupName => {
          this.myCurrentWay.groupName = groupName;
          this.wayName = groupName;
          this.setNakaGroupNameValue(groupName);
          this.groupService.changeGroupName(groupName);
          this.showGroupSettingErrorMsg = false;
          this.handleSuccessMessageAction('CHANGE_WAY_NAME', this.myCurrentWay.groupName);
        }, error => {
          this.handleError(error);
        }
      );
  }

  removeAsAdmin(userId, username) {
    this.removeAdminRightsService = this.groupService.removeAdminRights(userId, this.myCurrentWay._id)
      .subscribe(
        wayRole => {
          console.log(wayRole);
          const userIndex = this.myCurrentWay.users.findIndex(user => user._id === userId);
          this.myCurrentWay.users[userIndex].groupRole = groupRole[groupRole.PARTICIPANT];
          this.members = this.myCurrentWay.users;
          this.showGroupSettingErrorMsg = false;
          this.initMembersFilter();
          this.handleSuccessMessageAction('MAKE_PARTICIPANT', this.myCurrentWay.groupName, username);
        }, error => {
          this.handleError(error);
        }
      );
  }

  addNewMember(userId, username) {
    console.log('add this member');
    this.addWayMemberService = this.groupService.addNewMember(userId, this.myCurrentWay._id)
      .subscribe(
        newMember => {
          console.log(newMember);
          this.myCurrentWay.users.push(newMember);
          this.members = this.myCurrentWay.users;
          this.initMembersFilter();
          this.filteredOptions = this.addMembers.valueChanges
            .pipe(
              startWith(''),
              switchMap(val => {
                return this.filter(this.addMembers.value);
              })
            );
          this.getNewMembersService = this.filteredOptions
            .subscribe(mySearchlist => {
                console.log(mySearchlist);
                this.rawfilteredOptions = mySearchlist;
                if (this.groupDetails) {
                  this.eventInBottomSheet.emit(true);
                }
              },
              error => {
              });
          this.handleSuccessMessageAction('ADD_MEMBER', this.myCurrentWay.groupName, username);
          this.showGroupSettingErrorMsg = false;
        }, error => {
          this.handleError(error);
        }
      );
  }

  makeAdmin(userId, username) {
    this.makeAdminService = this.groupService.makeWayAdmin(userId, this.myCurrentWay._id)
      .subscribe(
        wayRole => {
          console.log(wayRole);
          const userIndex = this.myCurrentWay.users.findIndex(user => user._id === userId);
          this.myCurrentWay.users[userIndex].groupRole = groupRole[groupRole.ADMIN];
          this.members = this.myCurrentWay.users;
          this.showGroupSettingErrorMsg = false;
          this.initMembersFilter();
          this.handleSuccessMessageAction('MAKE_ADMIN', this.myCurrentWay.groupName, username);
        }, error => {
          this.handleError(error);
        }
      );
  }

  handleError(error) {
    if (error && error.status === 412) {
      this.errorMessage = error.error;
      this.showGroupSettingErrorMsg = true;
    } else if (error && error.status === 405) {
      this.errorMessage = error.error;
      this.showGroupSettingErrorMsg = true;
    } else if (error && error.status === 406) {
      this.errorMessage = error.error;
      this.showGroupSettingErrorMsg = true;
    } else {
      this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
      this.showGroupSettingErrorMsg = true;
    }
  }

  handleSuccessMessageAction(action, groupName, memberName?) {
    switch (action) {
      case 'ADD_MEMBER': {
        this.showActionSuccess(memberName + ' is now part of ' + groupName.toUpperCase());
        break;
      }
      case 'DELETE_MEMBER': {
        this.showActionSuccess(memberName + ' is no longer part of ' + groupName.toUpperCase());
        break;
      }
      case 'MAKE_ADMIN': {
        this.showActionSuccess(memberName + ' is now ADMIN of ' + groupName.toUpperCase());
        break;
      }
      case 'MAKE_PARTICIPANT': {
        this.showActionSuccess(memberName + ' is no longer ADMIN of ' + groupName.toUpperCase());
        break;
      }
      case 'CHANGE_WAY_NAME': {
        this.showActionSuccess('The way name has been changed to: ' + groupName.toUpperCase());
        break;
      }
    }
  }

  // Notification to confirm success on edit action
  showActionSuccess(message) {
    this.toastr.success(message, '', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 4000,
    });
  }

  openGroupAvatarDialog(): void {
    const dialogRef = this.dialog.open(ChangeGroupAvatarDialogComponent, {
      width: '550px',
      autoFocus: false,
      data: {wayInfo: this.myCurrentWay}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result && result.hasAvatar) {
        this.myCurrentWay.hasAvatar = true;
        this.myCurrentWay.avatarUrl = result.avatarUrl;
      } else if (result && result.error === true) {
        this.getGroupInfo();
      } else if (result && result.hasAvatar === false) {
        this.myCurrentWay.hasAvatar = false;
        this.myCurrentWay.avatarUrl = result.avatarUrl;
      }
    });
  }

  openRemoveWayMemberDialog(memberId, memberUsername): void {
    const dialogRef = this.dialog.open(RemoveWayMemberDialogComponent, {
      width: '550px',
      autoFocus: false,
      data: {memberId: memberId, memberUsername: memberUsername, groupName: this.myCurrentWay.groupName, wayId: this.myCurrentWay._id}
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result && result.deleteConfirmation === true) {
        this.myCurrentWay.users.splice(this.myCurrentWay.users.findIndex(user => user._id === memberId), 1);
        this.members = this.myCurrentWay.users;
        this.showGroupSettingErrorMsg = false;
        this.initMembersFilter();
        this.handleSuccessMessageAction('DELETE_MEMBER', this.myCurrentWay.groupName, memberUsername);
        if (this.groupDetails) {
          this.eventInBottomSheet.emit(true);
        }
      }  else if (result && result.deleteConfirmation === false && result.error) {
        this.handleError(result.error);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.getGroupInfoService) {
      this.getGroupInfoService.unsubscribe();
    }
    if (this.updateWayNameService) {
      this.updateWayNameService.unsubscribe();
    }
    if (this.removeAdminRightsService) {
      this.removeAdminRightsService.unsubscribe();
    }
    if (this.makeAdminService) {
      this.makeAdminService.unsubscribe();
    }
    if (this.updateGroupService) {
      this.updateGroupService.unsubscribe();
    }
    if (this.getNewMembersService) {
      this.getNewMembersService.unsubscribe();
    }
  }

}
