import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CalendarDateFormatter, CalendarEvent, CalendarEventTimesChangedEvent, CalendarView} from 'angular-calendar';
import {Event, Group} from '../../../_models';
import {Subject, Subscription} from 'rxjs';
import {addHours, addMinutes, startOfDay} from 'date-fns';
import {MatDatepicker, MatDatepickerInputEvent, MatDialog, MatMenuTrigger, MatSnackBar} from '@angular/material';
import {FormControl} from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import * as _moment from 'moment';
import {default as _rollupMoment, Moment} from 'moment';
import {CustomDateFormatter} from './custom-date-formatter.provider';
import {EventService} from '../../../_services/event.service';
import {GroupService} from '../../../_services/group.service';
import {CalendarEventEditDialogComponent} from './calendar-event-edit-dialog/calendar-event-edit-dialog.component';

const moment = _rollupMoment || _moment;
export const MY_FORMATS = {
  parse: {
    dateInput: 'MMMM YYYY',
  },
  display: {
    dateInput: 'MMMM YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    }
  ],
})
export class CalendarComponent implements OnInit, OnDestroy {
  innerWidth: any;
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  isGroupSelected = localStorage.getItem('myWay') !== null;
  selectedType = 'Month';
  viewDate: Date = new Date();
  endDateBeforeStartDate = false;
  refresh: Subject<any> = new Subject();
  createNewEventService: Subscription;
  editEventService: Subscription;
  deleteEventService: Subscription;
  updateGroupService: Subscription;
  getNewEventService: Subscription;
  scrollTowardEventService: Subscription;
  myCurrentWay: Group;
  eventEdition: Boolean = false;
  hasBeenInitiated = false;
  newEvent: Event = {
    title: '',
    start: new Date(),
    end: new Date(),
    groupId: localStorage.getItem('myWay')
  };
  eventClone: string;
  eventRequestError: Boolean = false;
  alert = [
    {value: 'NONE', viewValue: 'None'},
    {value: 'ONTIME', viewValue: 'At the time of the event'},
    {value: '5MIN', viewValue: '5 minutes before'},
    {value: '15MIN', viewValue: '15 minutes before'},
    {value: '30MIN', viewValue: '30 minutes before'},
    {value: '1HOUR', viewValue: '1 hour before'},
    {value: '1DAY', viewValue: '1 day before'},
    {value: '2DAYS', viewValue: '2 days before'}
  ];
  events: Event[] = [];
  eventNumber: Number = 0;
  @ViewChild(MatMenuTrigger) newEventMenu: MatMenuTrigger;
  activeDayIsOpen: Boolean = false;
  date = new FormControl(moment());
  scrollToEventId: Number = -1;

  constructor(private groupService: GroupService,
              private eventService: EventService,
              public dialog: MatDialog,
              private snackBar: MatSnackBar,
              private myElement: ElementRef) {
    this.onResize();
  }

  ngOnInit() {
    console.log('calendar inside ngOnInit');
    this.innerWidth = window.innerWidth;
    this.getEvents();
    this.updateCurrentGroupEventId();
    this.updateGroup();
  }

  /** Used to reload GroupInfo if user go to another way through search bar */
  updateGroup() {
    if (this.updateGroupService === undefined) {
      this.updateGroupService = this.groupService.currentGroup.subscribe((group) => {
          this.myCurrentWay = group;
          if (group !== null && this.hasBeenInitiated) {
            console.log('inside Update Group');
            this.getEvents();
          }
        },
        error => {
        });
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.innerWidth = window.innerWidth;
  }

  getEvents() {
    if (this.isGroupSelected) {
      this.getNewEventService = this.eventService.getEvents(localStorage.getItem('myWay'))
        .subscribe(
          events => {
            events.forEach(event => {
              event.start = new Date(event.start);
              event.end = new Date(event.end);
            });
            this.eventNumber = events.length;
            console.log(events);
            this.events = events;
            this.refresh.next();
            this.hasBeenInitiated = true;
            if (this.scrollToEventId !== -1) {
              this.scrollToAnEvent();
            }
          },
          error => {
          }
        );
    }
  }

  dateDiff(first, second) {
    return Math.floor(( second - first ) / 86400000);
  }

  toggleView(val: string) {
    this.selectedType = val;
  }

  menuClosed() {
    this.newEvent = {
      title: '',
      start: new Date(),
      end: new Date(),
      groupId: localStorage.getItem('myWay')
    };
    this.eventRequestError = false;
    this.eventEdition = false;
  }

  menuOpened(date) {
    if (!this.eventEdition) {
      const nowMinutes = addHours(startOfDay(date), new Date().getHours());
      this.newEvent.start = addMinutes(nowMinutes, new Date().getMinutes());
      this.newEvent.end = addHours(this.newEvent.start, 1);
    }
  }

  dateDiffMoment(first, second) {
    return moment(second).diff(moment(first, 'days'));
  }

  goToDayView(date) {
    this.viewDate = date;
    this.view = CalendarView.Day;
    this.selectedType = 'Day';
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    console.log(this.innerWidth);
    if (this.innerWidth <= 850) {
      this.viewDate = date;
      this.view = CalendarView.Day;
    }
  }

  uiPicker(type: string, event: MatDatepickerInputEvent<Date>) {
    this.viewDate = event.value;
  }

  chosenMonthHandler(normlized: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normlized.month());
    ctrlValue.year(normlized.year());
    this.viewDate = ctrlValue.toDate();
    datepicker.close();
  }

  displayEvenTitle(title) {
    if (this.innerWidth >= 1276) {
      if (title.length > 25) {
        return title.substring(0, 23).concat('...');
      } else {
        return title;
      }
    } else if (this.innerWidth < 1276 && this.innerWidth >= 1150) {
      if (title.length > 20) {
        return title.substring(0, 20).concat('...');
      } else {
        return title;
      }
    } else if (this.innerWidth < 1150 && this.innerWidth >= 950) {
      if (title.length > 16) {
        return title.substring(0, 16).concat('...');
      } else {
        return title;
      }
    } else if (this.innerWidth < 950 && this.innerWidth >= 850) {
      if (title.length > 14) {
        return title.substring(0, 13).concat('...');
      } else {
        return title;
      }
    }
  }

  startHour(date) {
    return moment(date).format('hh:mma') + ' ';
  }

  hourIds(date) {
    return moment(date).format('hh a');
  }

  updateCurrentGroupEventId() {
    if (this.scrollTowardEventService === undefined) {
      this.scrollTowardEventService = this.eventService.currentGroupEventId.subscribe((eventId) => {
          console.log(eventId);
          this.scrollToEventId = eventId;
        },
        error => {
        });
    }
  }

  scrollToAnEvent() {
          console.log(this.scrollToEventId);
          const calendar = this;
            const index = this.events.findIndex(event => event._id === this.scrollToEventId);
            console.log(index);
            if (this.events.findIndex(event => event._id === this.scrollToEventId) !== -1) {
              this.viewDate = calendar.events[index].start;
              this.view = CalendarView.Day;
              this.selectedType = 'Day';
              this.events[index].cssClass = 'highlightDayEvent';

              setTimeout(function () {
                if (calendar.myElement.nativeElement.ownerDocument.getElementById(moment(calendar.events[index].start).format('hh a'))) {
                  calendar.myElement.nativeElement.ownerDocument
                    .getElementById(moment(calendar.events[index].start)
                      .format('hh a'))
                    .scrollIntoView({behavior: 'smooth'});
                }
              }, 300);
              setTimeout(function () {
                delete calendar.events[index].cssClass;
              }, 4000);
            } else {
              console.log('insidElse');
                calendar.openSnackBar('Sorry, it seems the event has been deleted 🤷‍', '');
            }
  }


  enDateBeforeStartDate() {
    console.log(this.newEvent.end);
    if (!this.newEvent.end) {
        this.newEvent.end = new Date(this.newEvent.start);
    } else {
      console.log('ni');
      this.endDateBeforeStartDate = this.newEvent.end.valueOf() < this.newEvent.start.valueOf();
      if (this.endDateBeforeStartDate) {
        this.newEvent.end = addHours(this.newEvent.start, 1);
      }
    }
  }

  createNewEvent() {
    this.createNewEventService = this.eventService.create(this.newEvent)
      .subscribe(
        newEvent => {
          console.log(newEvent);
          this.addEvent(newEvent);
          this.newEventMenu.closeMenu();
        },
        error => {
          this.eventRequestError = true;
        }
      );
  }

  editCalendarEvent() {
    this.editEventService = this.eventService.update(this.newEvent)
      .subscribe(
        editedEvent => {
          console.log(editedEvent);
          this.addEditedEvent(editedEvent);
          this.newEventMenu.closeMenu();
        },
        error => {
          this.eventRequestError = true;
        }
      );
  }

  deleteCalendarEvent() {
    this.deleteEventService = this.eventService.removeEvent(this.newEvent._id)
      .subscribe(
        deletedEvent => {
          this.removeEvent(this.newEvent);
          this.newEventMenu.closeMenu();
        },
        error => {
          this.eventRequestError = true;
        }
      );
  }

  noDateUndefined() {
    if (!this.newEvent.start) {
      this.newEvent.start = addHours(this.newEvent.end, -1);
    } else {
      this.newEvent.end = addHours(this.newEvent.start, 1);
    }
  }

  eventTimesChanged({event, newStart, newEnd}: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event): void {
    console.log(action);
    if (action === 'Clicked') {
      console.log('clicketiiiii');
      this.newEvent = event;
      this.openCalendarEventDialog('EDIT');
    } else if (action === 'Dropped or resized') {
      this.editEventService = this.eventService.update(event)
        .subscribe(
          editedEvent => {
            console.log(editedEvent);
            this.openSnackBar('Event Saved 💾', '');
          },
          error => {
            this.eventRequestError = true;
          }
        );
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, '', {
      duration: 6000,
    });
  }

  addEvent(event): void {
    event.start = new Date(event.start);
    event.end = new Date(event.end);
    this.events.push(event);
    this.refresh.next();
  }

  addEditedEvent(event): void {
    const index = this.events.findIndex((eve) => eve._id === event._id);
    console.log('index is : ' + index);
    event.start = new Date(event.start);
    event.end = new Date(event.end);
    this.events[index] = event;
    this.refresh.next();
  }

  removeEvent(event): void {
    const index = this.events.findIndex((eve) => eve._id === event._id);
    console.log('index is : ' + index);
    this.events.splice(index, 1);
    this.refresh.next();
  }

  replacer(key, value) {
    if (value !== '') {
      return value;
    }
  }

  compare(): boolean {
    return this.eventClone === JSON.stringify(this.newEvent, this.replacer);
  }

  editEvent(event) {
    this.eventClone = JSON.stringify(event, this.replacer);
    this.eventEdition = true;
    this.newEvent = event;
    // this.newEventMenu.openMenu();
  }

  openCalendarEventDialog(type): void {
    const dialogRef = this.dialog.open(CalendarEventEditDialogComponent, {
      width: '550px',
      autoFocus: false,
      restoreFocus: false,
      panelClass: 'nakamaListTransparant',
      data: {newEvent: this.newEvent, type: type}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.menuClosed();
      if (result && result.action) {
        if (result.type === 'DELETE') {
          this.removeEvent(result.data);
        } else if (result.type === 'EDIT') {
          this.addEditedEvent(result.data);
        } else if (result.type === 'NEWEVENT') {
          this.addEvent(result.data);
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.eventService.changeGroupEventId(-1);
    if (this.createNewEventService) {
      this.createNewEventService.unsubscribe();
    }
    if (this.getNewEventService) {
      this.getNewEventService.unsubscribe();
    }
    if (this.updateGroupService) {
      this.updateGroupService.unsubscribe();
    }
    if (this.editEventService) {
      this.editEventService.unsubscribe();
    }
    if (this.deleteEventService) {
      this.deleteEventService.unsubscribe();
    }
    if (this.scrollTowardEventService) {
      this.scrollTowardEventService.unsubscribe();
    }
  }

}
