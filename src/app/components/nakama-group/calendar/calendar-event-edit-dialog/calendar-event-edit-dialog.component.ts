import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Subscription} from 'rxjs';
import {EventService} from '../../../../_services/event.service';
import {addHours, addMinutes, startOfDay} from 'date-fns';
import {Event} from '../../../../_models';

@Component({
  selector: 'app-calendar-event-edit-dialog',
  templateUrl: './calendar-event-edit-dialog.component.html',
  styleUrls: ['./calendar-event-edit-dialog.component.scss']
})
export class CalendarEventEditDialogComponent implements OnInit, OnDestroy {
  createNewEventService: Subscription;
  editEventService: Subscription;
  deleteEventService: Subscription;
  endDateBeforeStartDate = false;
  eventRequestError: Boolean = false;
  alert = [
    {value: 'NONE', viewValue: 'None'},
    {value: 'ONTIME', viewValue: 'At the time of the event'},
    {value: '5MIN', viewValue: '5 minutes before'},
    {value: '15MIN', viewValue: '15 minutes before'},
    {value: '30MIN', viewValue: '30 minutes before'},
    {value: '1HOUR', viewValue: '1 hour before'},
    {value: '1DAY', viewValue: '1 day before'},
    {value: '2DAYS', viewValue: '2 days before'},
    {value: 'ONEWEEK', viewValue: '1 week before'}
  ];
  eventClone: string;

  constructor(public dialogRef: MatDialogRef<CalendarEventEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private eventService: EventService) { }

  ngOnInit() {
    if (this.data.type === 'EDIT') {
      this.eventClone = JSON.stringify(this.data.newEvent, this.replacer);
    } else if (this.data.type === 'NEWEVENT') {
      this.data.newEvent.start = new Date();
      this.data.newEvent.end = addHours(new Date(), 1);
    }
  }

  noDateUndefined() {
    if (!this.data.newEvent.start) {
      this.data.newEvent.start = addHours(this.data.newEvent.end, -1);
    } else {
      this.data.newEvent.end = addHours(this.data.newEvent.start, 1);
    }
  }

  enDateBeforeStartDate() {
    if (!this.data.newEvent.end) {
      this.data.newEvent.end = new Date(this.data.newEvent.start);
    } else {
      this.endDateBeforeStartDate = this.data.newEvent.end.valueOf() < this.data.newEvent.start.valueOf();
      if (this.endDateBeforeStartDate) {
        this.data.newEvent.end = addHours(this.data.newEvent.start, 1);
      }
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  replacer(key, value) {
    if (value !== '') {
      return value;
    }
  }

  deleteCalendarEvent() {
    this.deleteEventService = this.eventService.removeEvent(this.data.newEvent._id)
      .subscribe(
        deletedEvent => {
          this.dialogRef.close({action: true, type: 'DELETE', data: this.data.newEvent});
        },
        error => {
          this.eventRequestError = true;
        }
      );
  }

  createNewEvent() {
    this.createNewEventService = this.eventService.create(this.data.newEvent)
      .subscribe(
        newEvent => {
          this.dialogRef.close({action: true, type: 'NEWEVENT', data: newEvent});
        },
        error => {
          this.eventRequestError = true;
        }
      );
  }

  editCalendarEvent() {
    this.editEventService = this.eventService.update(this.data.newEvent)
      .subscribe(
        editedEvent => {
          this.dialogRef.close({action: true, type: 'EDIT', data: editedEvent});
        },
        error => {
          this.eventRequestError = true;
        }
      );
  }

  compare(): boolean {
    return this.eventClone === JSON.stringify(this.data.newEvent, this.replacer);
  }

  ngOnDestroy(): void {
    if (this.createNewEventService) {
      this.createNewEventService.unsubscribe();
    }
    if (this.editEventService) {
      this.editEventService.unsubscribe();
    }
    if (this.deleteEventService) {
      this.deleteEventService.unsubscribe();
    }
  }

}
