import {
  AfterViewChecked,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ChatRoomService} from '../../../_services/chat-room.service';
import {Observable, Subscription} from 'rxjs';
import {WebSocketService} from '../../../_services/web-socket.service';
import {ChatMessage, Group, MessageFile, User} from '../../../_models';
import * as moment from 'moment';
import {DocUploadDialogComponent} from '../share-docs/doc-upload-dialog/doc-upload-dialog.component';
import {MatDialog} from '@angular/material';
import {GroupService} from '../../../_services/group.service';
import {FileUploader} from 'ng2-file-upload';
import {environment} from '../../../../environments/environment';
import {faFile} from '@fortawesome/free-solid-svg-icons';
import {DeleteFileComponent} from '../share-docs/delete-file/delete-file.component';
import {GetFileDataDialogComponent} from '../share-docs/get-file-data-dialog/get-file-data-dialog.component';
import {FileService} from '../../../_services/file.service';
import saveAs from 'file-saver';
import {Link, LinkType, NgxLinkifyjsService} from 'ngx-linkifyjs';
import {NakamaDetailDialogComponent} from '../../../nakama-detail-dialog/nakama-detail-dialog.component';


@Component({
  selector: 'app-nakama-chat',
  templateUrl: './nakama-chat.component.html',
  styleUrls: ['./nakama-chat.component.scss']
})
export class NakamaChatComponent implements OnInit, OnDestroy, AfterViewChecked {
  @ViewChildren('divfile') divfile: QueryList<ElementRef>;
  @ViewChild('messageList') private myScrollContainer: ElementRef;
  messageLoading = false;
  loadFirstDate = false;
  scrollToBottom = false;
  messagePreview: string;
  pastScrollRef;
  message = new FormControl;
  messageNumber = 30;
  theme: String = 'WHITE';
  link = 'https://www.youtube.com/embed/Wimkqo8gDZ0?controls=0&showinfo=0&rel=0&autoplay=1&mute=1&loop=1&playlist=Wimkqo8gDZ0';
  getChatRoomChatService: Subscription;
  updateUnreadMessageService: Subscription;
  getNewChatMessageService: Subscription;
  getMetaUpdateService: Subscription;
  getConnectedUsersService: Subscription;
  getReceiveTypingService: Subscription;
  getReceiveStopTypingService: Subscription;
  downloadFileService: Subscription;
  updateGroupService: Subscription;
  getMoreMessageService: Subscription;
  getMyChatDataService: Subscription;
  getMyChatLinksService: Subscription;
  chatLinksLoaded: Boolean = false;
  isGroupSelected = localStorage.getItem('myWay') !== null;
  firstCall: Boolean = false;
  loadingMoreMessage: Boolean = false;
  messages: ChatMessage[] = [];
  chatLinks: ChatMessage[] = [];
  isTyping = false;
  typingUser = new User();
  onlyNamamojis = false;
  openConnectedUsers = true;
  me: User;
  myCurrentWay: Group;
  userMessageListYPosition: number;
  currentMessageListHeight: number;
  newMessage: Boolean = false;
  newMetaMessage: Boolean = false;
  newFileMessage: Boolean = false;
  typingTimer = null;
  faFile = faFile;
  hasBeenInitiated = false;
  groupInitialized = false;
  personalDataLoaded = false;
  innerWidth: any;
  loadedImg = 0;
  imgLoadOnInit = false;
  loadedVideo = 0;
  bottomReachedOnOnit = false;
  videoLoadedOnInit = false;
  docMovieRootUrl = environment.serverHttp + '/filemovie/';
  sharDocsUrl = environment.serverHttp + '/file';
  docRootUrl = environment.serverHttp + '/file/';
  firstFoundedLink: Link;
  isFocused = true;
  lastGroupId: string;
  public hasBaseDropZoneOver: Boolean = false;
  @ViewChild('docUpload') private docUpload: ElementRef;
  public uploader: FileUploader = new FileUploader({
    url: this.sharDocsUrl,
    itemAlias: 'file',
  });
  connectedUsers: User[] = [];
  emojiRegex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|[\ud83c[\ude50\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (event.shiftKey && event.key === 'Enter') {
      return true;
    } else if (event.key === 'Enter') {
      event.preventDefault();
      return false;
    }
  }
  @HostListener('window:blur')
  isNotFocus() {
    this.isFocused = false;
  }
  @HostListener('window:focus')
  isFocus() {
    this.isFocused = true;
    document.title = 'NakamaWay';
  }


  constructor(
    private myElement: ElementRef,
    private chatRoomService: ChatRoomService,
    private groupService: GroupService,
    private webSocketService: WebSocketService,
    public dialog: MatDialog,
    private fileService: FileService,
    public linkifyService: NgxLinkifyjsService,
  ) {
    this.initChatRoomWebSocket();
    this.onResize();
    this.bottomReachedOnOnit = false;
  }

  ngOnInit() {
    this.getChatRoomChat();
    this.joinRoom();
    this.updateUnreadMessage();
    this.getMyChatInfo();
    this.initTextAreaHeight();
    this.preventEmptyMessage();
    this.updateGroup();
    this.innerWidth = window.innerWidth;
  }

  joinRoom() {
    if (this.isGroupSelected) {
      console.log('room joined');
      this.webSocketService.joinRoom({groupId: localStorage.getItem('myWay')});
    }
    }

  leaveRoom() {
    if (this.isGroupSelected) {
      console.log('room left');
      this.webSocketService.leaveRoom({groupId: localStorage.getItem('myWay')});
    }
  }

  updateGroup() {
    if (!this.updateGroupService) {
      this.updateGroupService = this.groupService.currentGroup.subscribe((group) => {
          console.log(group);
          this.myCurrentWay = group;
          if (group && localStorage.getItem('myWay') !== this.lastGroupId) {
            console.log('leave ze room');
            this.webSocketService.leaveRoom({groupId: this.lastGroupId});
          }
          if (group !== null) {
            this.groupInitialized = true;
          }
          if (group !== null && this.hasBeenInitiated) {
            console.log('inside cat update group');
            this.getChatRoomChat();
            this.joinRoom();
            this.updateUnreadMessage();
          }
        },
        error => {
        });
    }
  }

  initTextAreaHeight() {
    const textarea = this.myElement.nativeElement.ownerDocument.getElementById('message');
    textarea.focus();
    textarea.oninput = function() {
      textarea.style.height = '20px';
      textarea.style.height = Math.min(textarea.scrollHeight, 120) + 'px';
    };
  }

  newDay(newest, latest) {
    const a = moment(newest);
    const b = moment(latest);
    a.isSame(b, 'day');
    return a.isSame(b, 'day');
  }

  getMyChatInfo() {
    this.getMyChatDataService = this.chatRoomService.getMyChatData()
      .subscribe(
        me => {
          this.me = me;
          this.personalDataLoaded = true;
          console.log(me);
        },
        error => {
        });
  }

  initChatRoomWebSocket() {
      this.getNewChatMessageService = this.webSocketService.newMessageReceived().subscribe(data => {
        this.messages.push(data);
        console.log(data);
        const elem = this.myElement.nativeElement.ownerDocument.getElementById('messageList');
        const max = elem.scrollHeight;
        if (data.userId !== this.me._id && max - this.userMessageListYPosition >= 120) {
          console.log('newMessage while scrolling around');
          this.scrollToBottom = true;
          // messagePreview
          if (data.file && data.file.length > 0) {
            this.messagePreview = `${data.username} sent a new ${this.fileType(data.file[0].filename).type} `;
          } else {
            this.messagePreview = data.message;
          }
        } else if (data.file && data.file.length > 0) {
          console.log('newFileMessage');
          this.newFileMessage = true;
        } else if (data.linkMetaData) {
          console.log('il y a des méteudateux direct dans lmessage');
          if (data.linkMetaData.ogVideo || data.linkMetaData.ogImage) {
            this.newMetaMessage = true;
          } else {
            elem.scrollTop = elem.scrollHeight;
          }
        } else {
          this.newMessage = true;
        }
        this.isTyping = false;
        if (!this.isFocused) {
          document.title = data.username + ' is talking';
        }
      });
    this.getMetaUpdateService = this.webSocketService.messageMetaUpdate().subscribe(data => {
      console.log(data);
      const elem = this.myElement.nativeElement.ownerDocument.getElementById('messageList');
      const max = elem.scrollHeight;
      if (data.userId !== this.me._id && max - this.userMessageListYPosition >= 120) {
        console.log('newMetaMessage while scrolling around');
        this.scrollToBottom = true;
        this.messagePreview = data.message;
      } else {
        console.log('il y a des méteudateux');
        if (data.linkMetaData.ogVideo || data.linkMetaData.ogImage) {
          this.newMetaMessage = true;
        } else {
          elem.scrollTop = elem.scrollHeight;
        }
      }
      const index = this.messages.findIndex(message => message.timestamp === data.timestamp);
      console.log(index);
      if (index !== -1) {
        this.messages[index].linkMetaData = data.linkMetaData;
        console.log(this.messages[index]);
      }
    });
    this.getConnectedUsersService = this.webSocketService.roomConnectedUsers().subscribe(data => {
      console.log('roomConnectedUsers!');
      console.log(data);
      this.connectedUsers = data;
    });
    this.getReceiveTypingService = this.webSocketService.receivedTyping().subscribe(data => {
      console.log(data);
      this.isTyping = data.isTyping;
      this.typingUser.hasAvatar = data.hasAvatar;
      this.typingUser.username = data.username;
      this.typingUser.avatarUrl = data.avatarUrl;

      console.log('someoneis typing');
      const elem = document.getElementById('messageList');
      const max = elem.scrollHeight;
      if (max - this.userMessageListYPosition >= 120) {
          console.log('someone is typing but I am scrolling so I do not comme back to bottom');
        } else {
        setTimeout(() => { elem.scrollTop = elem.scrollHeight; }, 100);
        }
    });
    this.getReceiveStopTypingService = this.webSocketService.receivedStopTyping().subscribe(bool => {
      console.log(bool);
      this.isTyping = bool.isTyping;
      console.log('someone stopped typing');
    });
  }

  getChatRoomChat() {
    if (this.isGroupSelected) {
      if (this.myCurrentWay) {
        this.lastGroupId = this.myCurrentWay._id.toString();
      } else {
        this.lastGroupId = localStorage.getItem('myWay');
      }
      this.getChatRoomChatService = this.chatRoomService.getChatRoomsChat(localStorage.getItem('myWay'), 0)
        .subscribe(
          chatRoomChat => {
            console.log(chatRoomChat);
            this.messages = chatRoomChat.sort((val1, val2) =>  val1.postedAt < val2.postedAt ? -1 : 1);
              this.firstCall = true;
              this.pastScrollRef = this.messages.length;
              this.hasBeenInitiated = true;
            if (this.messages.length < this.messageNumber) {
              this.loadFirstDate = true;
            }
          },
          error => {
          });
    }
  }

  linksMenuOpened() {
    this.getMyChatLinks();
  }

  linksMenuClosed() {
    this.chatLinksLoaded = false;
  }

  getMyChatLinks() {
    if (this.isGroupSelected) {
      this.getMyChatLinksService = this.chatRoomService.getMyChatLinks(localStorage.getItem('myWay'))
        .subscribe(
          chatLinks => {
            console.log(chatLinks);
            this.chatLinks = chatLinks;
            this.chatLinksLoaded = true;
          },
          error => {});
    }
  }

  updateUnreadMessage() {
    this.updateUnreadMessageService = this.chatRoomService.updateMyNotifReadStatus(localStorage.getItem('myWay'))
      .subscribe(
        updatedMessages => {console.log(updatedMessages)},
        error => {});
  }

  loadMoreMessage() {
    const chat = this;
    if (this.isGroupSelected) {
      this.getChatRoomChatService = this.chatRoomService.getChatRoomsChat(localStorage.getItem('myWay'), this.messages.length)
        .subscribe(
          chatRoomChat => {
            console.log(chatRoomChat);
            chatRoomChat.forEach(function (message) {
              chat.messages.unshift(message);
            });
            // this.messages.push(chatRoomChat.sort((val1, val2) =>  val1.postedAt < val2.postedAt ? -1 : 1));
            this.loadingMoreMessage = true;
            this.messageLoading = false;
          },
          error => {
          });
    }
  }

  preventEmptyMessage() {
    this.message.valueChanges
      .subscribe ( value => {
        if (value && value.trim().length === 0) {
          this.message.setValue('');
        }
        // Check if this.message contains only emojis or regular character too in order to rise font-size
        this.onlyNamamojis = this.message.value && this.message.value.replace(this.emojiRegex, '').length === 0;
        const textarea = this.myElement.nativeElement.ownerDocument.getElementById('message');
        if (this.onlyNamamojis) {
          textarea.style.height = '52px';
        }
        // update this.foundLinks in order to activate open-graph-scraper on back-end size
        this.firstFoundedLink = this.linkifyService.find(value).find(link => {
          return link.type === LinkType.URL;
        });
        // console.log(this.firstFoundedLink);
      } );
  }

  onlyMojisString(string) {
    return string && string.replace(this.emojiRegex, '').length === 0;
  }

  handleScroll() {
    const elem = document.getElementById('messageList');
    const top = elem.scrollTop;
    const max = elem.scrollHeight;
    this.currentMessageListHeight = max;
    this.userMessageListYPosition = elem.scrollTop + elem.offsetHeight;
    // Handle onTopReached
    if (top === 0) {
      if (this.messageNumber <= this.messages.length) {
        this.messageLoading = true;
        this.messageNumber = this.messageNumber + 30;
        this.loadFirstDate = false;
        this.loadMoreMessage();
      } else {
        this.loadFirstDate = true;
      }
    }
    // Handle onBottomReached
    if (Math.round(this.userMessageListYPosition) === (max)) {
      this.scrollToBottom = false;
    }
  }

  scrollTowardBottom() {
    const elem = document.getElementById('messageList');
    elem.scrollTop = elem.scrollHeight;
  }

  navWhiteBack() {
    document.body.classList.remove('blackTheme');
    const elem = document.getElementById('appnavbar');
    console.log(elem);
    if (elem) {
      elem.classList.add('whiteBackGound');
    }
  }

  handleBlackTheme() {
      document.body.classList.add('blackTheme');
  }

  handleTheme() {
    if (this.theme === 'WHITE') {
      this.theme = 'BLACK';
      this.handleBlackTheme();
    } else if (this.theme === 'BLACK') {
      this.theme = 'WHITE';
      this.navWhiteBack();
    }
  }

  navTransparent() {
    document.body.classList.remove('blackTheme');
    const elem = document.getElementById('appnavbar');
    console.log(elem);
    if (elem) {
      elem.classList.remove('whiteBackGound');
    }
  }


  scrollToRef() {
    const elem = this.myElement.nativeElement.ownerDocument.getElementById(this.messages.length - this.pastScrollRef);
    const chatBox = this.myElement.nativeElement.ownerDocument.getElementById('messageList');
    console.log('elem res is :');
    console.log(elem);
    elem.scrollIntoView();
    chatBox.scrollBy(0, -40);
    this.loadingMoreMessage = false;
    this.pastScrollRef = this.messages.length;
  }

  checkLoadedImg() {
    const elem = this.myElement.nativeElement.ownerDocument.getElementById('messageList');
    const imgArray = document.getElementsByClassName('chatImg');
    const hasVideoArray = document.getElementsByClassName('videoResp').length > 0;
    this.loadedImg++;
    console.log(imgArray.length);
    console.log(this.loadedImg);
    if (this.loadedImg === imgArray.length && !this.imgLoadOnInit) {
      // this.firstCall = false;
      this.imgLoadOnInit = true;
      if (hasVideoArray && this.videoLoadedOnInit) {
        console.log('VideoAndImageLoadedOnInit');
        this.bottomReachedOnOnit = true;
        elem.scrollTop = elem.scrollHeight;
        this.firstCall = false;
      } else if (!hasVideoArray) {
        console.log('ImgLoadedOnInit');
        elem.scrollTop = elem.scrollHeight;
        this.bottomReachedOnOnit = true;
        this.firstCall = false;
      }
    } else if (this.newFileMessage && this.loadedImg === imgArray.length) {
      console.log('newImgMessage');
      // this.firstCall = false;
      this.newFileMessage = false;
      elem.scrollTop = elem.scrollHeight;
    }
  }

  checkLoadedMetaImg() {
    const elem = this.myElement.nativeElement.ownerDocument.getElementById('messageList');
    if (this.newMetaMessage || this.firstCall) {
      console.log('newMetaImgMessage');
      this.newMetaMessage = false;
      elem.scrollTop = elem.scrollHeight;
    }
}

  checkLoadedMetaVideo() {
    const elem = this.myElement.nativeElement.ownerDocument.getElementById('messageList');
    if (this.newMetaMessage || this.firstCall) {
      console.log('newMetaVideoMessage');
      this.newMetaMessage = false;
      setTimeout(() => elem.scrollTop = elem.scrollHeight, 300);
    }
  }

  checkIfImgSrcIsCorrect(imgSrc: string): boolean {
    return imgSrc.startsWith('http');
  }

  checkLoadedVideo() {
    const elem = this.myElement.nativeElement.ownerDocument.getElementById('messageList');
    const videoArray = document.getElementsByClassName('videoResp');
    const hasImgArray = document.getElementsByClassName('chatImg').length > 0;
    this.loadedVideo++;
    console.log(videoArray.length);
    console.log(this.loadedVideo);
    if (this.loadedVideo === videoArray.length && !this.videoLoadedOnInit) {
      // this.firstCall = false;
      this.videoLoadedOnInit = true;
      if (hasImgArray && this.imgLoadOnInit) {
        console.log('VideoAndImageLoadedOnInit');
        elem.scrollTop = elem.scrollHeight;
        this.bottomReachedOnOnit = true;
        this.firstCall = false;
      } else if (!hasImgArray) {
        console.log('VideoLoadedOnInit');
        elem.scrollTop = elem.scrollHeight;
        this.bottomReachedOnOnit = true;
        this.firstCall = false;
      }
    } else if (this.newFileMessage && this.loadedVideo === videoArray.length) {
      // this.firstCall = false;
      console.log('newVideoMessage');
      this.newFileMessage = false;
      elem.scrollTop = elem.scrollHeight;
    }
  }

  isArray(value: any): boolean {
      return Array.isArray(value);
  }

  ngAfterViewChecked(): void {
    const elem = this.myElement.nativeElement.ownerDocument.getElementById('messageList');
    const imgArray = document.getElementsByClassName('chatImg');
    const videoArray = document.getElementsByClassName('videoResp');
    const videoOnInit = document.getElementsByClassName('videoResp').length > 0;
    const imgOnInit = document.getElementsByClassName('chatImg').length > 0;
    const chat = this;
    if (elem && !videoOnInit && !imgOnInit && this.firstCall) {
      setTimeout(() => {
        this.bottomReachedOnOnit = true;
      });
      elem.scrollTop = elem.scrollHeight;
      this.firstCall = false;
      this.imgLoadOnInit = true;
      this.videoLoadedOnInit = true;
    }
    if (this.loadingMoreMessage) {
      this.scrollToRef();
    }
    if (this.newFileMessage) {
      let fileTypChecker = 0;
      this.messages[this.messages.length - 1].file.forEach((file, i) => {
        console.log(file);
        console.log(i);
        if (this.fileType(file.filename).type === 'file') {
          console.log('un type file');
          fileTypChecker++;
          if (i === this.messages[this.messages.length - 1].file.length - 1 &&
            fileTypChecker === this.messages[this.messages.length - 1].file.length) {
            console.log('all my files are fileType, I go to bottom');
            this.newFileMessage = false;
            elem.scrollTop = elem.scrollHeight;
            fileTypChecker = 0;
          }
        }
      });
    }
    if (this.newMessage) {
      console.log('new Message dans lafterviewchecked');
      this.newMessage = false;
      console.log('un message arrive alors qu je suis en bas');
      elem.scrollTop = elem.scrollHeight;
    }
  }

  sendMessage() {
    console.log('inside send message');
    if (this.message.value) {
      this.webSocketService.sendMessage(
        {
          groupId: localStorage.getItem('myWay'),
          username: this.me.username,
          userAvatar: this.me.avatarUrl,
          postedAt: moment.utc(new Date()).local().format(),
          message: this.message.value.replace(/^\s+|\s+$/g, ''),
          userId: this.me._id,
          link: this.firstFoundedLink
        });
      this.message.setValue('');
      this.myElement.nativeElement.ownerDocument.getElementById('message').style.height = '25px';
    }
  }

  addEmoji(e) {
    this.message.setValue(this.message.value ? this.message.value + e.emoji.native : e.emoji.native);
  }

  typing() {
    clearTimeout(this.typingTimer);
    this.typingTimer = setTimeout(() => this.stopTyping(), 1500);
    this.webSocketService.typing({
      groupId: localStorage.getItem('myWay'),
      username: this.me.username,
      avatarUrl: this.me.avatarUrl,
      hasAvatar: this.me.hasAvatar});
  }

  stopTyping() {
    console.log('inside stop typing');
    this.webSocketService.stopTyping({groupId: localStorage.getItem('myWay'), username: this.me.username});
  }

  fileChangeEvent(event: any): void {
    console.log('event');
    console.log(event);
    if (event.target.files.length > 0) {
        this.openShareDocsUploadDialog(event.target.files);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth <= 570) {
      this.openConnectedUsers = false;
    }
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public dropped(e: any): void {
    const file = [];
    if (e.length > 0) {
      console.log(this.uploader.queue);
      this.uploader.queue.forEach((item) => {
        file.push(item._file);
      });
      this.openShareDocsUploadDialog(file);
    }
  }

  openShareDocsUploadDialog(files): void {
    const dialogRef = this.dialog.open(DocUploadDialogComponent, {
      width: '700px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {group: this.myCurrentWay, files: files, addMessage: true}
    });
    dialogRef.afterClosed().subscribe(docUploaded => {
      const filesUploaded = [];
      const elem = this.myElement.nativeElement.ownerDocument.getElementById('fileUploadInput');
      elem.reset();
      this.uploader.clearQueue();
      this.fixButtonRippleEffectBug(this.docUpload);
      if (docUploaded && docUploaded.uploadedItems && docUploaded.uploadedItems.length > 0) {
        console.log(docUploaded);
        docUploaded.uploadedItems.forEach((doc) => {
          filesUploaded.push(new MessageFile(doc._id, doc.originalname, doc.mimetype, doc.filename, doc.size, doc.postedBy));
        });
        this.sendMessageWithFile(filesUploaded, docUploaded.message);
      }
    });
  }

  sendMessageWithFile(filesUploaded, message) {
    console.log('inside send message with File');
    console.log(filesUploaded);
      this.webSocketService.sendMessage(
        {
          groupId: localStorage.getItem('myWay'),
          username: this.me.username,
          userAvatar: this.me.avatarUrl,
          postedAt: moment.utc(new Date()).local().format(),
          message: message ? message.replace(/^\s+|\s+$/g, '') : '',
          file: filesUploaded,
          userId: this.me._id,
        });
  }

  openDeleteFileDialog(file, messageTimestamp): void {
    const dialogRef = this.dialog.open(DeleteFileComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {originalname: file.originalname, file: file, updateChat: true, groupId: localStorage.getItem('myWay')}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      console.log(confirm);
      if (confirm && confirm.updatedMessage.action === 'UPDATE') {
        this.deleteFileMessage(messageTimestamp, file.fileId);
      }
    });
  }

  openFileInfoDialog(fileId, fileType): void {
    const dialogRef = this.dialog.open(GetFileDataDialogComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {
        groupId: localStorage.getItem('myWay'),
        fileId: fileId,
        fileType: fileType
      }
    });
    dialogRef.afterClosed().subscribe(docUploaded => {
    });
  }

  openNakamaDetailDialog(avatarUrl, username): void {
    const dialogRef = this.dialog.open(NakamaDetailDialogComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {avatarUrl: avatarUrl, username: username}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      // this.fixButtonRippleEffectBug(this.buttonDeleteNakama);
    });
  }

  downloadFile(fileName, mimetype, originalName) {
    this.downloadFileService = this.fileService.downloadMyFile(fileName)
      .subscribe((file) => {
        const blob = new Blob([file], {type: mimetype});
        saveAs(blob, originalName);
      }, error => {
      });
  }

  deleteFileMessage(messageTimestamp, fileId) {
    const index = this.messages.findIndex(message => message.timestamp === messageTimestamp);
    if (index !== -1) {
      if (this.messages[index].file.length === 1 && this.messages[index].file[0].fileId === fileId) {
        this.messages.splice(index, 1);
      } else if (this.messages[index].file.length > 1) {
        const fileIndex = this.messages[index].file.findIndex(file => file.fileId === fileId);
        if (index !== -1) {
          this.messages[index].file.splice(fileIndex, 1);
        }
      }
    }
  }

  getExtension(filename) {
    const parts = filename.split('.');
    return parts[parts.length - 1];
  }

  fileType(filename) {
    if (this.isOther(filename)) {
      return {
        icon: true,
        type: 'file',
        faFa: this.faFile
      };
    } else if (this.isVideo(filename)) {
      return {
        icon: false,
        type: 'video',
        link: this.docMovieRootUrl + filename
      };
    } else if (this.isImage(filename)) {
      return {
        icon: false,
        type: 'img',
        link: this.docRootUrl + filename
      };
    }
  }

  isImage(filename) {
    const ext = this.getExtension(filename);
    switch (ext.toLowerCase()) {
      case 'jpg':
      case 'gif':
      case 'bmp':
      case 'png':
      case 'jpeg':
        return true;
    }
    return false;
  }

  isOther(filename) {
    return (
      !this.isImage(filename) &&
      !this.isVideo(filename));
  }

  isVideo(filename) {
    const ext = this.getExtension(filename);
    switch (ext.toLowerCase()) {
      case 'mov':
      case 'mp4':
      case 'ogg':
        return true;
    }
    return false;
  }

  fixButtonRippleEffectBug(button) {
    button._elementRef.nativeElement.classList.remove('cdk-program-focused');
    button._elementRef.nativeElement.classList.add('cdk-mouse-focused');
  }

  ngOnDestroy(): void {
    if (this.theme === 'CLOUD') {
      this.navWhiteBack();
    } else if (this.theme === 'BLACK') {
      document.body.classList.remove('blackTheme');
    }
    this.leaveRoom();
    if (this.getMyChatDataService) {
      this.getMyChatDataService.unsubscribe();
    }
    if (this.getChatRoomChatService) {
      this.getChatRoomChatService.unsubscribe();
    }
    if (this.getMoreMessageService) {
      this.getMoreMessageService.unsubscribe();
    }
    if (this.updateGroupService) {
      this.updateGroupService.unsubscribe();
    }
    if (this.downloadFileService) {
      this.downloadFileService.unsubscribe();
    }
    if (this.getNewChatMessageService) {
      this.getNewChatMessageService.unsubscribe();
    }
    if (this.getMetaUpdateService) {
      this.getMetaUpdateService.unsubscribe();
    }
    if (this.getReceiveTypingService) {
      this.getReceiveTypingService.unsubscribe();
    }
    if (this.getReceiveStopTypingService) {
      this.getReceiveStopTypingService.unsubscribe();
    }
    if (this.getConnectedUsersService) {
      this.getConnectedUsersService.unsubscribe();
    }
    if (this.updateUnreadMessageService) {
      this.updateUnreadMessageService.unsubscribe();
    }
    if (this.getMyChatLinksService) {
      this.getMyChatLinksService.unsubscribe();
    }
  }


}
