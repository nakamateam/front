import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NakamaChatComponent } from './nakama-chat.component';

describe('NakamaChatComponent', () => {
  let component: NakamaChatComponent;
  let fixture: ComponentFixture<NakamaChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NakamaChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NakamaChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
