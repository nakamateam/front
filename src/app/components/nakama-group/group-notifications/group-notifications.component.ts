import {Component, OnDestroy, OnInit} from '@angular/core';
import {Group, NotificationPreferences, NotifPrefTypes} from '../../../_models';
import {NakamaDetailDialogComponent} from '../../../nakama-detail-dialog/nakama-detail-dialog.component';
import {MatDialog} from '@angular/material';
import {GroupService} from '../../../_services/group.service';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-group-notifications',
  templateUrl: './group-notifications.component.html',
  styleUrls: ['./group-notifications.component.scss']
})
export class GroupNotificationsComponent implements OnInit, OnDestroy {
  updateGroupService: Subscription;
  getGroupInfoService: Subscription;
  updateGroupPrefService: Subscription;
  myCurrentWay: Group;
  myPreferences: NotificationPreferences;
  dataLoaded = false;
  isGroupSelected = localStorage.getItem('myWay') !== null;
  showGroupSettingErrorMsg: Boolean = false;
  errorMessage: string;
  notifPrefType = NotifPrefTypes;

  constructor(private groupService: GroupService,
              public dialog: MatDialog,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.updateGroup();
  }

  /** Used to reload GroupInfo if user go to another way through search bar */
  updateGroup() {
    this.updateGroupService = this.groupService.currentGroup.subscribe((group) => {
        console.log(group);
        this.myCurrentWay = group;
        if (group !== null) {
          this.getGroupNotificationSettings();
        }
      },
      error => {
        this.handleError(error);
      });
  }

  getGroupNotificationSettings() {
    this.dataLoaded = false;
    if (this.isGroupSelected) {
      this.getGroupInfoService = this.groupService.getGroupPref(localStorage.getItem('myWay'))
        .subscribe((grouPrefs) => {
            console.log(grouPrefs);
            this.myPreferences = grouPrefs;
            this.dataLoaded = true;
          },
          error => {
            this.handleError(error);
          });
    }
  }

  updateGrouPrefs(notifPrefType, newPrefUpdate) {
    console.log(notifPrefType);
    console.log(newPrefUpdate);
    this.updateGroupPrefService = this.groupService.updateGroupPref(localStorage.getItem('myWay'), notifPrefType, newPrefUpdate)
      .subscribe((newGrouPrefs) => {
          console.log(newGrouPrefs);
          this.myPreferences = newGrouPrefs;
        },
        error => {
          this.handleError(error);
        });
  }

  updateWayNotification(notifPrefType, e) {
        this.updateGrouPrefs(notifPrefType, e.checked);
  }

  handleError(error) {
    if (error && error.status === 405) {
      this.errorMessage = error.error;
      this.showGroupSettingErrorMsg = true;
    } else if (error && error.status === 412) {
      this.errorMessage = error.error;
      this.showGroupSettingErrorMsg = true;
    } else {
      this.errorMessage = 'An error has occured, please try again. If the problem persists contact nakamacharli@gmail.com.';
      this.showGroupSettingErrorMsg = true;
    }
  }


  openNakamaDetailDialog(avatarUrl): void {
    const dialogRef = this.dialog.open(NakamaDetailDialogComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {avatarUrl: avatarUrl}
    });
    dialogRef.afterClosed().subscribe(confirm => {
    });
  }

  ngOnDestroy(): void {
    if (this.getGroupInfoService) {
      this.getGroupInfoService.unsubscribe();
    }
    if (this.updateGroupService) {
      this.updateGroupService.unsubscribe();
    }
    if (this.updateGroupPrefService) {
      this.updateGroupPrefService.unsubscribe();
    }
  }

}
