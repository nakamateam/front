import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';
import {FileService} from '../../../../_services/file.service';

@Component({
  selector: 'app-delete-file',
  templateUrl: './delete-file.component.html',
  styleUrls: ['./delete-file.component.scss']
})
export class DeleteFileComponent implements OnInit, OnDestroy {

  deleteFileService: Subscription;
  deleteFileAndUpdateChatService: Subscription;
  errorMessage: string;

  constructor(public dialogRef: MatDialogRef<DeleteFileComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private fileService: FileService,
              private toastr: ToastrService) { }

  ngOnInit() {
    console.log(this.data);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  deleteFile() {
    this.deleteFileService = this.fileService.removeFile(this.data.file._id, this.data.groupId)
      .subscribe(
        deletedFileName => {
            this.deleteFileSuccess(deletedFileName);
            this.dialogRef.close({deletedFileId: this.data.file._id});
        },
        error => {
          console.log(error);
          if (error && error.status === 405) {
            this.errorMessage = error.error;
          }
        });
  }

  deleteFileAndUpdateChat() {
    this.deleteFileAndUpdateChatService = this.fileService.removeFileAndUpdateChat(this.data.file.fileId, this.data.groupId)
      .subscribe(
        updatedMessage => {
          this.deleteFileSuccess(this.data.file.originalname);
          this.dialogRef.close({deletedFileId: this.data.file.fileId, updatedMessage});
        },
        error => {
          console.log(error);
          if (error && error.status === 405) {
            this.errorMessage = error.error;
          }
        });
  }

  deleteFileSuccess(fileOriginalName) {
    this.toastr.success( 'The file ' + fileOriginalName + ' as been deleted', '', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 4000,
    });
  }

  ngOnDestroy(): void {
    if (this.deleteFileService) {
      this.deleteFileService.unsubscribe();
    }
    if (this.deleteFileAndUpdateChatService) {
      this.deleteFileAndUpdateChatService.unsubscribe();
    }
  }

}
