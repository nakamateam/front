import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FileService} from '../../../../_services/file.service';
import {Subscription} from 'rxjs';
import {File} from '../../../../_models/file';
import {HttpErrorResponse} from '@angular/common/http';
import {NakamaDetailDialogComponent} from '../../../../nakama-detail-dialog/nakama-detail-dialog.component';


@Component({
  selector: 'app-get-file-data-dialog',
  templateUrl: './get-file-data-dialog.component.html',
  styleUrls: ['./get-file-data-dialog.component.scss']
})
export class GetFileDataDialogComponent implements OnInit {
  getFileData: Subscription;
  fileInfo = new File();

  constructor(public dialogRef: MatDialogRef<GetFileDataDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private fileService: FileService,
              public dialog: MatDialog) { }

  ngOnInit() {
    console.log(this.data);
    this.getDataFile(this.data.fileId);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getDataFile(fileId) {
    this.getFileData = this.fileService.getFileDatas(fileId, this.data.groupId)
      .subscribe(
        fileData => {
          console.log(fileData);
          this.fileInfo = fileData;
        },
        error => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 401) {
              this.dialogRef.close();
            }
          }
        });
  }

  openNakamaDetailDialog(avatarUrl): void {
    const dialogRef = this.dialog.open(NakamaDetailDialogComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {avatarUrl: avatarUrl}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      // this.fixButtonRippleEffectBug(this.buttonDeleteNakama);
    });
  }

}
