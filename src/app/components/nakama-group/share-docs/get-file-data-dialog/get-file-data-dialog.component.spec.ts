import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetFileDataDialogComponent } from './get-file-data-dialog.component';

describe('GetFileDataDialogComponent', () => {
  let component: GetFileDataDialogComponent;
  let fixture: ComponentFixture<GetFileDataDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetFileDataDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetFileDataDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
