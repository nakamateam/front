import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatSnackBar} from '@angular/material';
import {DocUploadDialogComponent} from './doc-upload-dialog/doc-upload-dialog.component';
import {Subscription} from 'rxjs';
import {GroupService} from '../../../_services/group.service';
import {Group} from '../../../_models';
import {FileService} from '../../../_services/file.service';
import {environment} from '../../../../environments/environment';
import {DeleteFileComponent} from './delete-file/delete-file.component';
import saveAs from 'file-saver';
import {faFile, faFileExcel, faFilePdf, faFilePowerpoint, faFileWord} from '@fortawesome/free-solid-svg-icons';
import {GetFileDataDialogComponent} from './get-file-data-dialog/get-file-data-dialog.component';

@Component({
  selector: 'app-share-docs',
  templateUrl: './share-docs.component.html',
  styleUrls: ['./share-docs.component.scss']
})
export class ShareDocsComponent implements OnInit, OnDestroy {
  updateGroupService: Subscription;
  scrollTowardFileService: Subscription;
  getFilesService: Subscription;
  downloadFileService: Subscription;
  myCurrentWay: Group;
  myFiles = [];
  filteredFiles = [];
  dataLoaded = false;
  isGroupSelected = localStorage.getItem('myWay') !== null;
  docRootUrl = environment.serverHttp + '/file/';
  docMovieRootUrl = environment.serverHttp + '/filemovie/';
  faFileExcel = faFileExcel;
  faFilePowerpoint = faFilePowerpoint;
  faFileWord = faFileWord;
  faFilePdf = faFilePdf;
  hasBeenInitiated = false;
  faFile = faFile;
  selectedType = 'All';
  scrollTowardFile: Boolean = false;
  loadingImg = '../../../../assets/BCoMax.png';
  imgCardDoc = 'imgCardDoc';
  scrollToGroupFileId: Number = -1;


  constructor(public dialog: MatDialog,
              private groupService: GroupService,
              private fileService: FileService,
              private myElement: ElementRef,
              private snackBar: MatSnackBar) {
  }

  @ViewChild('docUpload') private docUpload: ElementRef;
  @ViewChild('downloadMobileButton') private downloadMobileButton: ElementRef;
  @ViewChild('highlight') public highlightedFile: ElementRef;

  ngOnInit() {
    this.getGroupFiles();
    this.updateCurrentGroupFileId();
    this.updateGroup();
  }

  getGroupFiles() {
    if (this.isGroupSelected) {
      this.getFilesService = this.fileService.getMyFiles(localStorage.getItem('myWay'))
        .subscribe((files) => {
            console.log(files);
            this.myFiles = files;
            this.filteredFiles = files;
            this.hasBeenInitiated = true;
            this.dataLoaded = true;
            if (this.scrollToGroupFileId !== -1) {
              this.scrollToAFile();
            }
          },
          error => {
          });
    }
  }

  updateGroup() {
    if (!this.updateGroupService) {
      this.updateGroupService = this.groupService.currentGroup.subscribe((group) => {
          console.log(group);
          this.myCurrentWay = group;
          if (group !== null && this.hasBeenInitiated) {
            this.getGroupFiles();
          }
        },
        error => {
        });
    }
  }

  updateCurrentGroupFileId() {
    if (!this.scrollTowardFileService) {
      this.scrollTowardFileService = this.fileService.currentGroupFileId.subscribe((fileId) => {
          console.log(fileId);
          this.scrollToGroupFileId = fileId;
        },
        error => {
        });
    }
  }

  scrollToAFile() {
        console.log(this.scrollToGroupFileId);
        const shareDocs = this;
          const index = this.filteredFiles.findIndex(file => file._id === this.scrollToGroupFileId);
          console.log(index);
          if (this.filteredFiles.findIndex(file => file._id === this.scrollToGroupFileId) !== -1) {
            this.scrollTowardFile = true;
            this.filteredFiles[index].highlight = true;
            setTimeout(function () {
              if (shareDocs.myElement.nativeElement.ownerDocument.getElementById('highlight')) {
                shareDocs.myElement.nativeElement.ownerDocument.getElementById('highlight').scrollIntoView({behavior: 'smooth'});
              }
            }, 300);
            setTimeout(function () {
              shareDocs.scrollTowardFile = false;
              delete shareDocs.filteredFiles[index].highlight;
            }, 6000);
          } else {
            console.log('insidElse');
            this.openSnackBar('Sorry, it seems this file has been deleted 🤷‍', '');
          }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, '', {
      duration: 2000,
    });
  }

  downloadFile(fileName, mimetype, originalName) {
    this.downloadFileService = this.fileService.downloadMyFile(fileName)
      .subscribe((file) => {
        const blob = new Blob([file], {type: mimetype});
        saveAs(blob, originalName);
      }, error => {
      });
  }

  filterByType(val: string) {
    console.log(val);
    this.selectedType = val;
    this.filterMyFiles();
  }

  filterMyFiles() {
    const filterFileByImg = this.myFiles.filter(file => this.isImage(file.originalname));
    const filterFileByVideo = this.myFiles.filter(file => this.isVideo(file.originalname));
    const filterFileByFile = this.myFiles.filter(file => this.isFile(file.originalname));
    if (this.selectedType === 'All') {
      this.filteredFiles = this.myFiles;
    } else if (this.selectedType === 'Image') {
      this.filteredFiles = filterFileByImg;
    } else if (this.selectedType === 'Video') {
      this.filteredFiles = filterFileByVideo;
    } else if (this.selectedType === 'File') {
      this.filteredFiles = filterFileByFile;
    }
  }

  fileChangeEvent(event: any): void {
    if (event.target.files.length > 0) {
      this.openShareDocsUploadDialog(event.target.files);
    }
  }

  openShareDocsUploadDialog(files): void {
    const dialogRef = this.dialog.open(DocUploadDialogComponent, {
      width: '700px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {group: this.myCurrentWay, files: files}
    });
    dialogRef.afterClosed().subscribe(docUploaded => {
      const elem = this.myElement.nativeElement.ownerDocument.getElementById('fileUploadInput');
      elem.reset();
      this.fixButtonRippleEffectBug(this.docUpload);
      this.fixButtonRippleEffectBug(this.downloadMobileButton);
      if (docUploaded && docUploaded.uploadedItems && docUploaded.uploadedItems.length > 0) {
        this.myFiles = this.myFiles.concat(docUploaded.uploadedItems);
        this.filteredFiles = this.filteredFiles.concat(docUploaded.uploadedItems);
        this.filterMyFiles();
      }
    });
  }

  openFileInfoDialog(fileId, fileType): void {
    const dialogRef = this.dialog.open(GetFileDataDialogComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {
        groupId: localStorage.getItem('myWay'),
        fileId: fileId,
        fileType: fileType
      }
    });
    dialogRef.afterClosed().subscribe(docUploaded => {
    });
  }

  openDeleteFileDialog(file): void {
    const dialogRef = this.dialog.open(DeleteFileComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {file: file, groupId: localStorage.getItem('myWay')}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm && confirm.deletedFileId) {
        console.log(confirm.deletedFileId);
        this.myFiles = this.myFiles.filter(function (value, index, arr) {
          return value._id !== confirm.deletedFileId;
        });
        this.filteredFiles = this.filteredFiles.filter(function (value, index, arr) {
          return value._id !== confirm.deletedFileId;
        });
      }
    });
  }

  getExtension(filename) {
    const parts = filename.split('.');
    return parts[parts.length - 1];
  }

  isImage(filename) {
    const ext = this.getExtension(filename);
    switch (ext.toLowerCase()) {
      case 'jpg':
      case 'gif':
      case 'bmp':
      case 'png':
      case 'jpeg':
        return true;
    }
    return false;
  }

  isExcel(filename) {
    const ext = this.getExtension(filename);
    switch (ext.toLowerCase()) {
      case 'xlsx':
      case 'xlsm':
      case 'xlsb':
      case 'xls':
      case 'csv':
      case 'xltx':
      case 'xltm':
      case 'xlt':
      case 'xml':
        return true;
    }
    return false;
  }

  isWord(filename) {
    const ext = this.getExtension(filename);
    switch (ext.toLowerCase()) {
      case 'docx':
      case 'docm':
      case 'dotx':
      case 'dotm':
      case 'dot':
      case 'odt':
      case 'doc':
        return true;
    }
    return false;
  }

  isPowerPoint(filename) {
    const ext = this.getExtension(filename);
    switch (ext.toLowerCase()) {
      case 'pptx':
      case 'pptm':
      case 'ppt':
      case 'odp':
      case 'potx':
      case 'potm':
      case 'pot':
        return true;
    }
    return false;
  }

  isVideo(filename) {
    const ext = this.getExtension(filename);
    switch (ext.toLowerCase()) {
      case 'mov':
      case 'mp4':
      case 'ogg':
        return true;
    }
    return false;
  }

  isPdf(filename) {
    const ext = this.getExtension(filename);
    switch (ext.toLowerCase()) {
      case 'pdf':
        return true;
    }
    return false;
  }

  isOther(filename) {
    return (
      !this.isExcel(filename) &&
      !this.isImage(filename) &&
      !this.isPowerPoint(filename) &&
      !this.isPdf(filename) &&
      !this.isWord(filename) &&
      !this.isVideo(filename));
  }

  isFile(filename) {
    return (
      !this.isImage(filename) &&
      !this.isVideo(filename));
  }

  fileType(filename) {
    if (this.isExcel(filename)) {
      return {
        icon: true,
        type: 'file',
        faFa: this.faFileExcel
      };
    } else if (this.isPowerPoint(filename)) {
      return {
        icon: true,
        type: 'file',
        faFa: this.faFilePowerpoint
      };
    } else if (this.isPdf(filename)) {
      return {
        icon: true,
        type: 'file',
        faFa: this.faFilePdf
      };
    } else if (this.isWord(filename)) {
      return {
        icon: true,
        type: 'file',
        faFa: this.faFileWord
      };
    } else if (this.isOther(filename)) {
      return {
        icon: true,
        type: 'file',
        faFa: this.faFile
      };
    } else if (this.isVideo(filename)) {
      return {
        icon: false,
        type: 'video',
        link: this.docMovieRootUrl + filename
      };
    } else if (this.isImage(filename)) {
      return {
        icon: false,
        type: 'img',
        link: this.docRootUrl + filename
      };
    }
  }

  fixButtonRippleEffectBug(button) {
    button._elementRef.nativeElement.classList.remove('cdk-program-focused');
    button._elementRef.nativeElement.classList.add('cdk-mouse-focused');
  }

  ngOnDestroy(): void {
    this.fileService.changeGroupFileId(-1);
    if (this.updateGroupService) {
      this.updateGroupService.unsubscribe();
    }
    if (this.getFilesService) {
      this.getFilesService.unsubscribe();
    }
    if (this.downloadFileService) {
      this.downloadFileService.unsubscribe();
    }
    if (this.scrollTowardFileService) {
      this.scrollTowardFileService.unsubscribe();
    }
  }
}
