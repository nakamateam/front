import {Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FileItem, FileUploader, ParsedResponseHeaders} from 'ng2-file-upload';
import {environment} from '../../../../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import {FormControl} from '@angular/forms';
import {Subscription} from 'rxjs';
import {GroupService} from '../../../../_services/group.service';

@Component({
  selector: 'app-doc-upload-dialog',
  templateUrl: './doc-upload-dialog.component.html',
  styleUrls: ['./doc-upload-dialog.component.scss']
})
export class DocUploadDialogComponent implements OnInit, OnDestroy {

  maxFileSize = 10 * 1024 * 1024;
  groupFileMaxSize = 1000 * 1024 * 1024;
  sharDocsUrl = environment.serverHttp + '/file';
  private keyUser = 'nakamaUser';
  currentUser = JSON.parse(localStorage.getItem(this.keyUser));
  public uploader: FileUploader = new FileUploader({
    url: this.sharDocsUrl,
    itemAlias: 'file',
    maxFileSize: this.maxFileSize,
    headers: [{name: 'x-xsrf-token', value: this.currentUser.token}]
  });
  public hasBaseDropZoneOver: Boolean = false;
  successUploadItems = [];
  errorUploadItems = [];
  allUploadFailed = false;
  errorMessage = '';
  onAddingItemFailed = false;
  getGroupFileSizeService: Subscription;
  message = new FormControl;

  constructor(public dialogRef: MatDialogRef<DocUploadDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private toastr: ToastrService,
              private groupService: GroupService) {
    this.uploader.onCompleteAll = () => {
      console.log('onCompleteAll');
      if (this.errorUploadItems.length === 0 && this.successUploadItems.length === 0) {
      } else if (this.errorUploadItems.length === 0) {
        if (!this.data.addMessage) {
          this.showFileUploadSuccess(this.successUploadItems.length.toString());
        }
        this.dialogRef.close({uploadedItems: this.successUploadItems, message: this.message.value});
      } else if (this.errorUploadItems.length > 0 && this.successUploadItems.length > 0) {
        if (!this.data.addMessage) {
          this.showFileUploadSuccess(this.successUploadItems.length.toString());
          this.listFailedUploadedFiles();
        }
        this.dialogRef.close({uploadedItems: this.successUploadItems, message: this.message.value});
      } else if (this.errorUploadItems.length > 0 && this.successUploadItems.length === 0) {
        this.allUploadFailed = true;
        console.log('oups, sound like nothing passes');
      }
    };
    this.uploader.onWhenAddingFileFailed = (item, filter) => {
      switch (filter.name) {
        case 'fileSize':
          this.onAddingItemFailed = true;
          this.errorMessage = 'Maximum upload size exceeded (' + Math.round( item.size / 1048576 * 10 ) / 10 + ' Mb of 10Mb allowed)';
          break;
        default:
          this.onAddingItemFailed = true;
          this.errorMessage = `Unknown error when adding the file`;
      }
    };
  }

  ngOnInit() {
    this.getGroupTotalFileSize();
    this.initUploader();
    if (this.data.files) {
      this.uploader.addToQueue(this.data.files);
    }
  }

  getGroupTotalFileSize() {
      this.getGroupFileSizeService = this.groupService.getMyGroupRole(localStorage.getItem('myWay'))
        .subscribe(group => {
          this.data.group = group;
        }, error => {
        });
  }

  initUploader() {
    this.uploader.onBeforeUploadItem = (item: FileItem) => {
      const wayIdHeader = this.uploader.options.headers.find(function (element) {
        return element.name === 'way-id';
      });
      if (!wayIdHeader) {
        const wayId = {name: 'way-id', value: this.data.group._id};
        this.uploader.options.headers.push(wayId);
      }
      this.uploader.options.additionalParameter = {
        wayId: this.data.group._id,
      };
    };
    this.uploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
    this.uploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    console.log('onSuccessItem');
    const newUploadedFile = JSON.parse(response);
    console.log(newUploadedFile);
    this.successUploadItems.push(newUploadedFile);
  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    console.log(item);
    this.errorUploadItems.push(item);
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  showFileUploadSuccess(successLoadedItems) {
    console.log(successLoadedItems);
    this.toastr.success(successLoadedItems  + this.fileOrFiles(successLoadedItems) + ' uploaded successfully', 'File upload', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 4000,
    });
  }

  showFileUploadError(errorLoadedItems, itemsFailedName) {
    this.toastr.error(errorLoadedItems  + this.fileOrFiles(errorLoadedItems) +
      ' has not been uploaded: ' + itemsFailedName, 'Files upload Error', {
      closeButton: true,
      positionClass: 'toast-bottom-left',
      timeOut: 6000,
    });
  }

  /** Init errorUploadItems if any */
  clearFailedItems() {
    if (this.allUploadFailed && this.errorUploadItems.length > 0) {
      this.allUploadFailed = false;
      this.errorUploadItems = [];
      this.uploader.clearQueue();
    }
    if (this.onAddingItemFailed) {
      this.onAddingItemFailed = false;
    }
  }

  fileOrFiles(successLoadedItems) {
    return (successLoadedItems === '1' ? ' file' : ' files');
  }

  listFailedUploadedFiles() {
    let uploadItemsFailed = '';
    const docUpload = this;
    this.errorUploadItems.forEach(function (file, i) {
      if (docUpload.errorUploadItems.length === 1) {
        uploadItemsFailed += (file.file.name);
      }
      if (docUpload.errorUploadItems.length > 1 && i !== docUpload.errorUploadItems.length - 1) {
        uploadItemsFailed += (file.file.name + ', ');
      }
      if (docUpload.errorUploadItems.length > 1 && i === docUpload.errorUploadItems.length - 1) {
        uploadItemsFailed += (file.file.name + '');
      }
      if (i === docUpload.errorUploadItems.length - 1) {
        console.log(uploadItemsFailed);
        docUpload.showFileUploadError(docUpload.errorUploadItems.length.toString(), uploadItemsFailed);
      }
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    if (this.getGroupFileSizeService) {
      this.getGroupFileSizeService.unsubscribe();
    }
  }

}
