import {Component, OnDestroy, OnInit} from '@angular/core';
import { slideInAnimation } from '../animations';
import {Router, RouterOutlet} from '@angular/router';
import {GroupService} from '../../_services/group.service';
import {Subscription} from 'rxjs';
import {Group} from '../../_models';

@Component({
  selector: 'app-nakama-group',
  templateUrl: './nakama-group.component.html',
  styleUrls: ['./nakama-group.component.scss'],
  animations: [slideInAnimation] // register the animation
})
export class NakamaGroupComponent implements OnInit, OnDestroy {
  updateGroupService: Subscription;
  getMyGroupInfo: Subscription;
  myCurrentWay: Group;

  constructor(private groupService: GroupService,
              private router: Router) {}

  ngOnInit() {
    this.updateGroupId();
  }

  updateGroupId() {
    console.log('my Current Group In LocalStorage is: ' + localStorage.getItem('myWay') !== undefined);
    this.updateGroupService = this.groupService.currentGroupId.subscribe((groupId) => {
      if (groupId !== -1) {
        this.getMyGroupInfo = this.groupService.getMyGroupRole(groupId)
          .subscribe(group => {
            console.log('groupId nest pas égal à -1');
            console.log(group);
            this.myCurrentWay = group;
            this.groupService.updateMyLastConnection(groupId).subscribe(lastConnectionDate => {
            });
            this.groupService.changeGroup(this.myCurrentWay);
            this.groupService.changeGroupName(this.myCurrentWay.groupName);
            if (group === null) {
              this.router.navigate(['home'])
                .then(function (safeAndSound) {
                });
            }
          }, error => {
          });
      } else if (localStorage.getItem('myWay') !== undefined) {
        this.getMyGroupInfo = this.groupService.getMyGroupRole(localStorage.getItem('myWay'))
          .subscribe(group => {
            console.log('groupId est égal à -1 mais il est bien dans le localStorage il sagit dun refresh');
            console.log(group);
            this.myCurrentWay = group;
            this.groupService.changeGroup(this.myCurrentWay);
            this.groupService.changeGroupName(this.myCurrentWay.groupName);
            if (group === null) {
              this.router.navigate(['home'])
                .then(function (safeAndSound) {
                });
            }
          }, error => {
          });
      } else {
        this.router.navigate(['home'])
          .then(function (safeAndSound) {
          });
      }
    },
      error => {
      });
    if (localStorage.getItem('myWay') === null || localStorage.getItem('myWay') === undefined) {
      this.router.navigate(['home'])
        .then(function (safeAndSound) {
        });
    }
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  ngOnDestroy(): void {
    this.groupService.removeCurrentGroup();
    if (this.updateGroupService) {
      this.updateGroupService.unsubscribe();
    }
    if (this.getMyGroupInfo) {
      this.getMyGroupInfo.unsubscribe();
    }
  }

}
