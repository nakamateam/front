import {Component, Input, OnInit} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-img-loader',
  templateUrl: './img-loader.component.html',
  styleUrls: ['./img-loader.component.scss']
})
export class ImgLoaderComponent implements OnInit {
  @Input() imgSrc: String;
  @Input() lastImg: String;
  @Input() spinnerSrc: String;
  @Input() imgContainerClass: String;
  docRootUrl = environment.serverHttp + '/file/';
  loading: Boolean = true;

  onLoad() {
    this.loading = false;
  }
  constructor() { }

  ngOnInit() {
  }

}
