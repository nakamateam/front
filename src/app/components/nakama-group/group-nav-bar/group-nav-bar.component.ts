import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {DeleteGroupComponent} from '../../home/delete-group/delete-group.component';
import {Subscription} from 'rxjs';
import {GroupService} from '../../../_services/group.service';
import {Group} from '../../../_models/group';
import {Router} from '@angular/router';

@Component({
  selector: 'app-group-nav-bar',
  templateUrl: './group-nav-bar.component.html',
  styleUrls: ['./group-nav-bar.component.scss']
})
export class GroupNavBarComponent implements OnInit, OnDestroy {

  updateGroupService: Subscription;
  myCurrentWay: Group;
  groupUpdated = false;

  constructor(public dialog: MatDialog,
              private groupService: GroupService,
              private router: Router) { }
  navLinks = [
    {path: '/way/chat', label: 'Chat', icon: 'chat'},
    {path: '/way/calendar', label: 'Calendar', icon: 'calendar_today'},
    {path: '/way/sharedocs', label: 'Files', icon: 'folder_shared'}
    ];

  ngOnInit() {
    this.updateGroup();
  }

  updateGroup() {
    if (!this.updateGroupService) {
      this.updateGroupService = this.groupService.currentGroup.subscribe((group) => {
          console.log(group);
          this.myCurrentWay = group;
          if (group !== null) {
            this.groupUpdated = true;
          }
        },
        error => {
        });
    }
  }

  openDeleteGroupDialog(groupId, groupName, isAdmin): void {
    const dialogRef = this.dialog.open(DeleteGroupComponent, {
      width: '550px',
      autoFocus: false,
      panelClass: 'my-full-screen-dialog',
      data: {groupId: groupId, groupName: groupName, isAdmin: isAdmin}
    });
    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm && confirm.deleteConfirmation) {
        this.router.navigate(['home'])
          .then(function (safeAndSound) {
          });
      }
    });
  }

  ngOnDestroy(): void {
    if (this.updateGroupService) {
      this.updateGroupService.unsubscribe();
    }
  }

}
