import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../_services';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router,
              private authenticationService: AuthService
              ) {}

  // routes to protect to admin account
  private adminRoutes: Array<string> = ['admindashboard'];

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authenticationService.isAuthenticated()) {
      if (this.isRouteAdmin(route) && !this.authenticationService.isAdmin()) {
        this.redirectHome(state);
        return false;
      }
      return true;
    }
    // not logged in so redirect to login page with the return url
    this.redirectLogin(state);
    return false;
  }

  /**
   * Redirect to login page
   * @param state RouterStateSnapshot
   */
  private redirectLogin(state: RouterStateSnapshot) {
    this.router.navigate(['/'], { queryParams: { returnUrl: state.url } });
  }

  /**
   * Redirect to Home page
   * @param state RouterStateSnapshot
   */
  private redirectHome(state: RouterStateSnapshot) {
    this.router.navigate(['/home'], { queryParams: { returnUrl: state.url } });
  }

  isRouteAdmin(route: ActivatedRouteSnapshot) {
    for (const index in this.adminRoutes) {
      if (this.adminRoutes[index] === route.routeConfig.path) {
        return true;
      }
    }
    return false;
  }

}
