import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../_services';

@Injectable()
export class LoginAuthGuard implements CanActivate {

  constructor(private router: Router,
              private authenticationService: AuthService
              ) {}

  private loginRoute: String = '';

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authenticationService.isAuthenticated() && route.routeConfig.path === this.loginRoute) {
        this.router.navigate(['/home']);
        return false;
    }
    return true;
  }

}
