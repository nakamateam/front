import { NgModule } from '@angular/core';
import { LoginComponent } from './components/login/login.component';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {AuthGuard} from './_guards/authGuard';
import {CalendarComponent} from './components/nakama-group/calendar/calendar.component';
import {AdminComponent} from './components/admin/admin.component';
import {NakamaGroupComponent} from './components/nakama-group/nakama-group.component';
import {NakamaChatComponent} from './components/nakama-group/nakama-chat/nakama-chat.component';
import {ShareDocsComponent} from './components/nakama-group/share-docs/share-docs.component';
import {ForgotPasswordComponent} from './components/login/forgot-password/forgot-password.component';
import {NakamaSettingsComponent} from './components/nakama-settings/nakama-settings.component';
import {ResetPasswordComponent} from './components/login/reset-password/reset-password.component';
import {GroupSettingsComponent} from './components/nakama-group/group-settings/group-settings.component';
import {GroupNotificationsComponent} from './components/nakama-group/group-notifications/group-notifications.component';
import {LoginAuthGuard} from './_guards/loginAuthGuard';

const appRoutes: Routes = [
  // routes + authentication.
  { path: '', component: LoginComponent, canActivate: [LoginAuthGuard] },
  { path: 'forgotpw', component: ForgotPasswordComponent },
  { path: 'resetpw', component: ResetPasswordComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'settings', component: NakamaSettingsComponent, canActivate: [AuthGuard] },
  {
    path: 'admindashboard',
    component: AdminComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'way',
    component: NakamaGroupComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'chat', component: NakamaChatComponent, canActivate: [AuthGuard], data: {animation: 'chatPage'}},
      { path: 'calendar', component: CalendarComponent, canActivate: [AuthGuard], data: {animation: 'calendarPage'}},
      { path: 'sharedocs', component: ShareDocsComponent, canActivate: [AuthGuard], data: {animation: 'docPage'}},
      { path: 'settings', component: GroupSettingsComponent, canActivate: [AuthGuard], data: {animation: 'groupSettings'}},
      { path: 'notifications', component: GroupNotificationsComponent, canActivate: [AuthGuard], data: {animation: 'notificationSettings'}},
    ]
  },
  {path: '**', redirectTo: 'home', canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(
    appRoutes,
  )],
  exports: [RouterModule],
})

export class AppRoutingModule { }
