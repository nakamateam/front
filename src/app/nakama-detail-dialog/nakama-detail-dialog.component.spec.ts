import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NakamaDetailDialogComponent } from './nakama-detail-dialog.component';

describe('NakamaDetailDialogComponent', () => {
  let component: NakamaDetailDialogComponent;
  let fixture: ComponentFixture<NakamaDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NakamaDetailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NakamaDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
