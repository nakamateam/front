import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-nakama-detail-dialog',
  templateUrl: './nakama-detail-dialog.component.html',
  styleUrls: ['./nakama-detail-dialog.component.scss']
})
export class NakamaDetailDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<NakamaDetailDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
