import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { AdminComponent } from './components/admin/admin.component';
import { CalendarComponent } from './components/nakama-group/calendar/calendar.component';
import { NakamaChatComponent } from './components/nakama-group/nakama-chat/nakama-chat.component';
import { AppNavbarComponent } from './components/app-navbar/app-navbar.component';
import { NakamaSettingsComponent } from './components/nakama-settings/nakama-settings.component';
import { NakamaGroupComponent } from './components/nakama-group/nakama-group.component';
import { ShareDocsComponent } from './components/nakama-group/share-docs/share-docs.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material-module';
import {DatePipe} from '@angular/common';
import {AuthGuard} from './_guards/authGuard';
import {AuthService} from './_services';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {Interceptor} from './_helpers/httpInterceptor';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AboutUsComponent } from './components/login/about-us/about-us.component';
import { ForgotPasswordComponent } from './components/login/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/login/reset-password/reset-password.component';
import {ToastrModule} from 'ngx-toastr';
import {SettingsService} from './_services/settings.service';
import { ChangeAvatarComponent } from './components/nakama-settings/change-avatar/change-avatar.component';
import {WebcamModule} from 'ngx-webcam';
import {ImageCropperModule} from 'ngx-image-cropper';
import {FileUploadModule} from 'ng2-file-upload';
import { SearchNakamaComponent } from './components/home/search-nakama/search-nakama.component';
import { CreateGroupComponent } from './components/home/create-group/create-group.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NotificationComponent } from './components/app-navbar/notification/notification.component';
import { DeleteNakamaComponent } from './components/home/delete-nakama/delete-nakama.component';
import { NakamaDetailDialogComponent } from './nakama-detail-dialog/nakama-detail-dialog.component';
import { DeleteGroupComponent } from './components/home/delete-group/delete-group.component';
import { SearchGroupComponent } from './components/app-navbar/search-group/search-group.component';
import { GroupNavBarComponent } from './components/nakama-group/group-nav-bar/group-nav-bar.component';
import { GroupSettingsComponent } from './components/nakama-group/group-settings/group-settings.component';
import { GroupTitleComponent } from './components/app-navbar/group-title/group-title.component';
import { DocUploadDialogComponent } from './components/nakama-group/share-docs/doc-upload-dialog/doc-upload-dialog.component';
import { SecurePipe } from './_pipe/secure.pipe';
import { DeleteFileComponent } from './components/nakama-group/share-docs/delete-file/delete-file.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { GetFileDataDialogComponent } from './components/nakama-group/share-docs/get-file-data-dialog/get-file-data-dialog.component';
import { GroupNotificationsComponent } from './components/nakama-group/group-notifications/group-notifications.component';
import { ChangeGroupAvatarDialogComponent } from './components/nakama-group/group-settings/change-group-avatar-dialog/change-group-avatar-dialog.component';
import { RemoveWayMemberDialogComponent } from './components/nakama-group/group-settings/remove-way-member-dialog/remove-way-member-dialog.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { FlatpickrModule } from 'angularx-flatpickr';
import { ImgLoaderComponent } from './components/nakama-group/img-loader/img-loader.component';
import { CalendarEventEditDialogComponent } from './components/nakama-group/calendar/calendar-event-edit-dialog/calendar-event-edit-dialog.component';
import 'hammerjs';
import { PickerModule } from '@ctrl/ngx-emoji-mart';
import { NgxLinkifyjsModule } from 'ngx-linkifyjs';
import { SafePipe } from './_pipe/safe.pipe';
import { GroupUsersBottomSheetComponent } from './components/home/group-users-bottom-sheet/group-users-bottom-sheet.component';
import {LoginAuthGuard} from './_guards/loginAuthGuard';
import { AddMemberComponent } from './components/admin/add-member/add-member.component';
import { ManageUserComponent } from './components/admin/manage-user/manage-user.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AdminComponent,
    CalendarComponent,
    NakamaChatComponent,
    AppNavbarComponent,
    NakamaSettingsComponent,
    NakamaGroupComponent,
    ShareDocsComponent,
    AboutUsComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    ChangeAvatarComponent,
    SearchNakamaComponent,
    CreateGroupComponent,
    NotificationComponent,
    DeleteNakamaComponent,
    NakamaDetailDialogComponent,
    DeleteGroupComponent,
    SearchGroupComponent,
    GroupNavBarComponent,
    GroupSettingsComponent,
    GroupTitleComponent,
    DocUploadDialogComponent,
    SecurePipe,
    DeleteFileComponent,
    GetFileDataDialogComponent,
    GroupNotificationsComponent,
    ChangeGroupAvatarDialogComponent,
    RemoveWayMemberDialogComponent,
    ImgLoaderComponent,
    CalendarEventEditDialogComponent,
    SafePipe,
    GroupUsersBottomSheetComponent,
    AddMemberComponent,
    ManageUserComponent
  ],
  entryComponents: [
    AboutUsComponent,
    ChangeAvatarComponent,
    CreateGroupComponent,
    DeleteNakamaComponent,
    NakamaDetailDialogComponent,
    DeleteGroupComponent,
    DocUploadDialogComponent,
    DeleteFileComponent,
    GetFileDataDialogComponent,
    ChangeGroupAvatarDialogComponent,
    RemoveWayMemberDialogComponent,
    CalendarEventEditDialogComponent,
    GroupUsersBottomSheetComponent,
    AddMemberComponent,
    ManageUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    HttpClientModule,
    FormsModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    WebcamModule,
    ImageCropperModule,
    FileUploadModule,
    NgSelectModule,
    FontAwesomeModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    FlatpickrModule.forRoot(),
    PickerModule,
    NgxLinkifyjsModule.forRoot({
      enableHash: false,
      enableMention: false
    })
  ],
  providers: [
    DatePipe,
    AuthGuard,
    LoginAuthGuard,
    AuthService,
    SettingsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
