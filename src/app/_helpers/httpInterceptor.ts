import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import {tap} from 'rxjs/operators';
import {WebSocketService} from '../_services/web-socket.service';

@Injectable()
export class Interceptor implements HttpInterceptor {

  constructor(private router: Router, private webSocketService: WebSocketService) {}

  private keyUser = 'nakamaUser';
  private ioToken = 'ioToken';
  private myWay = 'myWay';

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add x-xsrf-token with jwt token if available
    const currentUser = JSON.parse(localStorage.getItem(this.keyUser));
    const ioToken = localStorage.getItem(this.ioToken);
    if (currentUser && currentUser.token) {
      request = request.clone({
        setHeaders: {
          'x-xsrf-token' : currentUser.token,
          'ioToken': ioToken,
        },
        withCredentials: true
      });
    } else {
      request = request.clone({
        withCredentials: true
      });
    }

    return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          this.redirectToLogin();
        }
      }
    })
    );
  }

  redirectToLogin() {
    // remove user from local storage to log user out
    localStorage.removeItem(this.keyUser);
    localStorage.removeItem(this.ioToken);
    localStorage.removeItem(this.myWay);
    this.webSocketService.closeSocket();
    this.router.navigate(['/']);
  }
}
