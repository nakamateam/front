export class NotificationPreferences {
  constructor() {}

  _id: number;
  allNotifOff: boolean;
  calendar: boolean;
  chat: boolean;
  settings: boolean;
  sharedFiles: boolean;
  }

export enum NotifPrefTypes {
  allNotifOff = 'allNotifOff',
  chat = 'chat',
  calendar = 'calendar',
  sharedFiles = 'sharedFiles',
  settings = 'settings'
}
