import {UserGroup} from './userGroup';
import {CreatorDetail} from './creatorDetail';

export class Group {
  constructor() {
    this.users = [];
  }

  _id: number;
  creatorId: number;
  groupName: string;
  users: Array<UserGroup>;
  creationDate: Date;
  hasAvatar: boolean;
  avatarUrl: string;
  isAdmin: boolean;
  isCreator: boolean;
  groupSize: number;
  creatorDetails: CreatorDetail;

}

