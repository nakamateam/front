import {Nakama} from './nakama';
import {Notifications} from './notification';

export class User {
  constructor() {
    this.role = UserRole.NAKAMA;
  }

  _id: number;
  username: string;
  firstName: string;
  lastName: string;
  password: string;
  email: string;
  token: string;
  ioToken: string;
  notifications: [Notifications];
  myNakamas: [Nakama];
  friend: User;
  status: string;
  nakamaId: number;
  hasAvatar: boolean;
  avatarUrl: string;
  role: UserRole;
  requestPending: boolean;
  alreadyNakama: boolean;
  isAlreadyMember: boolean;
  requestYou: boolean;

  static parseUserRole(role: string) {
    switch (role) {
      case 'NAKAMADMIN' : return UserRole.NAKAMADMIN;
      case 'NAKAMA': return UserRole.NAKAMA;
      default: return UserRole.NAKAMA;
    }
  }
}

export enum UserRole {
  NAKAMADMIN,
  NAKAMA,
}

export interface IUserResponse {
  total: number;
  results: User[];
}
