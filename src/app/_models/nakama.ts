export class Nakama {
  constructor() {}

  _id: number;
  nakamaId: number;
  statusType: STATUSTYPES;

  static parseNakamaStatus(status: string) {
    switch (status) {
      case 'NAKAMAREQUESTPENDING' : return STATUSTYPES.NAKAMAREQUESTPENDING;
      case 'NAKAMA': return STATUSTYPES.NAKAMA;
      default: return STATUSTYPES.NAKAMAREQUESTPENDING;
    }
  }
}

export enum STATUSTYPES {
  NAKAMAREQUESTPENDING,
  NAKAMA,
}
