export class UserGroup {
  constructor(_id) {
    this.userId = _id;
    this.joinDate = new Date();
  }

  _id: number;
  userId: number;
  groupRole: string;
  joinDate: Date;
  lastConnectionDate: Date;

  static parseUserGroupStatus(status: string) {
    switch (status) {
      case 'ADMIN' : return groupRole.ADMIN;
      case 'PARTICIPANT': return groupRole.PARTICIPANT;
      default: return groupRole.PARTICIPANT;
    }
  }
}

export enum groupRole {
  ADMIN,
  PARTICIPANT,
}
