import {User} from './user';
import {NotifMetaData} from './notifMetaData';
import {Group} from './group';

export class Notifications {
  constructor() {}

  _id: number;
  sender: number;
  senderInfo: User;
  receiverInfo: User;
  unRead: boolean;
  nakamaId: number;
  timeSinceNotif: string;
  created_at: Date;
  groupInfo: Group;
  metaData: NotifMetaData;
  notificationType: NOTIFTYPES;

  static parseNotifType(notif: string) {
    switch (notif) {
      case 'INITIATENOTIF' : return NOTIFTYPES.INITIATENOTIF;
      case 'NAKAMAREQUEST' : return NOTIFTYPES.NAKAMAREQUEST;
      case 'NEWNAKAMACONFIRMATION': return NOTIFTYPES.NEWNAKAMACONFIRMATION;
      case 'NEWGROUPINVITATION': return NOTIFTYPES.NEWGROUPINVITATION;
      case 'REQUESTCANCELED': return NOTIFTYPES.REQUESTCANCELED;
      case 'NEWFILEUPLOADED': return NOTIFTYPES.NEWFILEUPLOADED;
      default: return NOTIFTYPES.NAKAMAREQUEST;
    }
  }
}

export enum NOTIFTYPES {
  INITIATENOTIF = 'INITIATENOTIF',
  NAKAMAREQUEST = 'NAKAMAREQUEST',
  NEWNAKAMACONFIRMATION = 'NEWNAKAMACONFIRMATION',
  NEWGROUPINVITATION = 'NEWGROUPINVITATION',
  REQUESTCANCELED = 'REQUESTCANCELED',
  NEWFILEUPLOADED = 'NEWFILEUPLOADED',
  NEWGROUPNAME = 'NEWGROUPNAME',
  NEWGROUPAVATAR = 'NEWGROUPAVATAR',
  NEWCALENDAREVENT = 'NEWCALENDAREVENT',
  CALENDAREVENTEDITED = 'CALENDAREVENTEDITED',
  NEWMESSAGE = 'NEWMESSAGE'
}
