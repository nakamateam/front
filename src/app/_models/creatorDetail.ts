export class CreatorDetail {
  constructor() {}

  _id: number;
  avatarUrl: string;
  firstName: string;
  lastName: string;
  username: string;

}
