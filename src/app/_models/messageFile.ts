export class MessageFile {
  constructor(fileId, originalname, mimetype, filename, size, postedBy) {
    this.fileId = fileId;
    this.originalname = originalname;
    this.mimetype = mimetype;
    this.filename = filename;
    this.size = size;
    this.postedBy = postedBy;
  }

  fileId: number;
  originalname: string;
  mimetype: string;
  filename: string;
  size: number;
  postedBy: number;
}
