import {CalendarEvent} from 'angular-calendar';

export interface Event extends CalendarEvent {
  title: string;
  start: Date;
  _id?: string | number;
  description?: string;
  place?: string;
  alert?: alert;
  groupId: string | number;
}

export enum alert {
  onTime = 'ONTIME',
  fiveMin = '5MIN',
  fifteeMin = '15MIN',
  thirtyMin = '30MIN',
  oneHour = '1HOUR',
  oneDay = '1DAY',
  twoDays = '2DAYS',
  oneWeek = 'ONEWEEK'
}
