export class File {
  constructor() {}

  _id: number;
  fieldname: string;
  originalname: string;
  postedBy: string;
  mimetype: string;
  destination: string;
  filename: string;
  path: string;
  size: number;
  creationDate: Date;
  postedByAvatarUrl: string;

}

