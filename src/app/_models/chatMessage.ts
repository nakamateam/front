import {MessageFile} from './messageFile';

export class ChatMessage {
  constructor() {}

  _id: number;
  groupId: number;
  userId: number;
  file: Array<MessageFile>;
  userAvatar: string;
  username: string;
  message: string;
  postedAt: Date;
  timestamp: string;
  linkMetaData: any;

}
