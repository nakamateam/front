export class NotifMetaData {
  constructor() {}

  groupId: number;
  groupName: string;
  isImage: boolean;
  filename: string;
  fileId: number;
  oldGroupName: string;
  newGroupName: string;
  eventId: number;
  eventStartDay: Date;
  eventTitle: string;
  alertType: string;
}
