import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {ChatMessage, User} from '../_models';

@Injectable({
  providedIn: 'root'
})
export class ChatRoomService {
  serverHttp: String = environment.serverHttp;

  constructor(private http: HttpClient) { }

  getChatRoomsChat(chatroom: string, fromMessage: number): Observable<any> {
    console.log(fromMessage);
    return this.http.get<any>(`${this.serverHttp}/chatroom/${chatroom}/${fromMessage}`);
  }

  getMyChatData(): Observable<User> {
    return this.http.get<User>(`${this.serverHttp}/chat-user`);
  }

  updateMyNotifReadStatus(chatRoomId): Observable<any> {
    return this.http.put<any>(`${this.serverHttp}/chat/clear/unread`, { roomId: chatRoomId });
  }

  getMyChatLinks(chatRoomId: string): Observable<ChatMessage[]> {
    return this.http.get<ChatMessage[]>(`${this.serverHttp}/chat-links/${chatRoomId}`);
  }
}
