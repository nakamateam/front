import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Notifications, NOTIFTYPES} from '../_models';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private defaultValue = NOTIFTYPES.INITIATENOTIF;
  private notificationSource = new BehaviorSubject<NOTIFTYPES>(this.defaultValue);
  liveNotif = this.notificationSource.asObservable();

  constructor(private http: HttpClient) {}

  changeLiveNotif(newNotif: NOTIFTYPES) {
    this.notificationSource.next(newNotif);
  }

  getMyNotifications(notifNumber: number): Observable<Notifications[]> {
    return this.http.get<Notifications[]>(environment.serverHttp + '/notification/' + notifNumber);
  }

  getMyUnreadMessage(chatRoomId): Observable<number> {
    return this.http.get<number>(environment.serverHttp + '/chat/unread/' + chatRoomId);
  }

  updateNotifications(nakamaId): Observable<Notifications[]> {
    return this.http.put<Notifications[]>(environment.serverHttp + '/notification', {nakamaIds: nakamaId});
  }
}
