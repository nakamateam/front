import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Group} from '../_models/group';
import {BehaviorSubject, Observable} from 'rxjs';
import {NotificationPreferences, User} from '../_models';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  mySelectedWay = 'myWay';
  private defaultValue = -1;
  private groupSourceId = new BehaviorSubject<number>(this.defaultValue);
  currentGroupId = this.groupSourceId.asObservable();

  private defaultGroupName = null;
  private groupNameSource = new BehaviorSubject<string>(this.defaultGroupName);
  currentGroupName = this.groupNameSource.asObservable();

  private defaultGroup = null;
  private groupSource = new BehaviorSubject<Group>(this.defaultGroup);
  currentGroup = this.groupSource.asObservable();

  changeGroup(group: Group) {
    this.groupSource.next(group);
  }

  changeGroupId(idGroup: number) {
    this.groupSourceId.next(idGroup);
  }

  changeGroupName(groupName: string) {
    this.groupNameSource.next(groupName);
  }

  removeCurrentGroup() {
      localStorage.removeItem(this.mySelectedWay);
      this.changeGroupName(this.defaultGroupName);
  }

  constructor(private http: HttpClient) { }

  search(query) {
    return this.http.post<any>(environment.serverHttp + '/group', {query: query});
  }

  create(group: Group) {
    return this.http.post<any>(environment.serverHttp + '/groups', group);
  }

  getMyGroupRole(id): Observable<Group> {
    return this.http.get<Group>(environment.serverHttp + '/group-role/' + id);
  }

  updateMyLastConnection(wayId): Observable<Group> {
    return this.http.put<Group>(environment.serverHttp + '/new-connection', {wayId: wayId});
  }

  getGroupInfo(id): Observable<Group> {
    return this.http.get<Group>(environment.serverHttp + '/group/' + id);
  }

  getGroupPref(id): Observable<NotificationPreferences> {
    return this.http.get<NotificationPreferences>(environment.serverHttp + '/group-preference/' + id);
  }

  updateGroupPref(id, notifPrefType: string, newPref: boolean): Observable<NotificationPreferences> {
    return this.http.put<NotificationPreferences>(environment.serverHttp + '/group-preference',
      {wayId: id, notifPrefType: notifPrefType, newPrefValue: newPref});
  }

  getMyGroups(): Observable<Group[]> {
    return this.http.get<Group[]>(environment.serverHttp + '/groups');
  }

  deleteGroup(groupId): Observable<any> {
    return this.http.put<any>(environment.serverHttp + '/group', {groupId: groupId});
  }

  removeGroupAvatar(wayId): Observable<any> {
    return this.http.put<any>(environment.serverHttp + '/group-avatar', {wayId: wayId});
  }

  updateWayName(newWayname, wayId): Observable<string> {
    return this.http.put<string>(environment.serverHttp + '/group-name', {newWayName: newWayname, wayId: wayId});
  }

  removeAdminRights(userId, wayId): Observable<any> {
    return this.http.put<any>(environment.serverHttp + '/make-group-participant', {userId: userId, wayId: wayId});
  }

  addNewMember(userId, wayId): Observable<any> {
    return this.http.put<any>(environment.serverHttp + '/new-member', {userId: userId, wayId: wayId});
  }

  makeWayAdmin(userId, wayId): Observable<any> {
    return this.http.put<any>(environment.serverHttp + '/make-group-admin', {userId: userId, wayId: wayId});
  }

  removeFromGroup(userId, wayId): Observable<any> {
    return this.http.put<any>(environment.serverHttp + '/remove-way-user', {userId: userId, wayId: wayId});
  }

  searchNewMember(query, wayId) {
    return this.http.post<any>(environment.serverHttp + '/search-new-member', {query: query, wayId: wayId});
  }

}
