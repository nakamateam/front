import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../_models';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private defaultValue = new User();
  private nakamaSource = new BehaviorSubject<User>(this.defaultValue);
  currentNakama = this.nakamaSource.asObservable();

  constructor(private http: HttpClient) { }

  changeNakama(nakama: User) {
    this.nakamaSource.next(nakama);
  }

  getNakamaDatas(): Observable<User> {
    return this.http.get<User>(environment.serverHttp + '/user');
  }

  getAvatar(): Observable<Blob> {
    return this.http.get(environment.serverHttp + '/avatar', { responseType: 'blob' });
  }

  updateNakaName(newNakaname): Observable<User> {
    return this.http.put<User>(environment.serverHttp + '/nakaname', newNakaname);
  }

  updateNakaMail(newNakaMail): Observable<User> {
    return this.http.put<User>(environment.serverHttp + '/nakamail', newNakaMail);
  }

  updateNakaPw(newNakaPw): Observable<User> {
    return this.http.put<User>(environment.serverHttp + '/nakapw', newNakaPw);
  }

  removeAvatar(): Observable<any> {
    return this.http.delete<any>(environment.serverHttp + '/avatar');
  }
}
