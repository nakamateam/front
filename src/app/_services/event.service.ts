import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Event} from '../_models';
import {environment} from '../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) { }

  private defaultValue = -1;
  private groupEventId = new BehaviorSubject<number>(this.defaultValue);
  currentGroupEventId = this.groupEventId.asObservable();

  changeGroupEventId(eventId: number) {
    this.groupEventId.next(eventId);
  }

  create(event: Event) {
    return this.http.post<any>(environment.serverHttp + '/events', event);
  }

  getEvents(groupId): Observable<Event[]> {
    return this.http.get<Event[]>(environment.serverHttp + '/events/' + groupId);
  }

  update(event: Event): Observable<Event> {
    return this.http.put<Event>(environment.serverHttp + '/events', event);
  }

  removeEvent(eventId: string|number): Observable<any> {
    return this.http.delete<any>(environment.serverHttp + '/event/' + eventId);
  }
}
