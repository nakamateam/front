import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import * as socketIo from 'socket.io-client';
import {ChatMessage, User} from '../_models';


@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  private initiateConnectedNakamaList = [];
  private connectedNakamaSource = new BehaviorSubject<String[]>(this.initiateConnectedNakamaList);
  myConnectedNakamas = this.connectedNakamaSource.asObservable();

  private socket;

  constructor() { }

  updateMyConnectedNakamas(myConnectedNakamas) {
    this.connectedNakamaSource.next(myConnectedNakamas);
  }

  public initSocket(): void {
    console.log(localStorage.getItem('ioToken'));
    this.socket = socketIo(environment.socket.baseUrl);
    const socketService = this;
    this.socket
      .on('connect', () => {
        this.socket.emit('authenticate', {token: localStorage.getItem('ioToken')});
      });
    // .emit('authenticate', {token: localStorage.getItem('ioToken')}) // send the jwt
    this.socket.on('authenticated', function () {
      console.log('authetifiééééé');
    });
    this.socket.on('unauthorized', function(error) {
      console.log('lost connection control');
      if (error.data.type === 'UnauthorizedError' || error.data.code === 'invalid_token') {
        // redirect user to login page perhaps?
        console.log('User token has expired');
      }
    });
    this.socket.on('connect_error', function () {
      // try to reconnect on socket network error
      console.log('oups');
      // socketOnError.emit('authenticate', {token: localStorage.getItem('ioToken')}); // send the jwt
    });
  }

  public closeSocket(): void {
    console.log('closSocketInNotifService');
    this.socket.disconnect();
  }

  public send(message: String): void {
    console.log('userId');
    this.socket.emit('userId', message);
  }

  public refreshConnectedUserList(): void {
    console.log('refreshConnectedUserList');
    this.socket.emit('refreshList');
  }

  public updateConnectedUsers(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('activeSocketUsers', (data: any) => observer.next(data));
    });
  }

  public getMyConnectedNakamaOnConnect(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('myConnectedNakamas', (data: any) => observer.next(data));
    });
  }

  public updateConnectedNakamas(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('NakamaConnection', (data: any) => observer.next(data));
    });
  }

  public updateDisonnectedNakamas(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('NakamaDisonnection', (data: any) => observer.next(data));
    });
  }

  public resetActiveNakamaUsersList(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('connect_error', (data: any) => observer.next(data));
    });
  }

  public onNotif(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('newNotif', (data: any) => observer.next(data));
    });
  }

  public onEventAlertNotif(): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on('newEventAlertNotif', (data: any) => observer.next(data));
    });
  }

  /** chat relative function */

  joinRoom(data) {
    console.log(data);
    this.socket.emit('join', data);
  }

  leaveRoom(data) {
    console.log(data);
    this.socket.emit('leave', data);
  }

  sendMessage(data) {
    this.socket.emit('message', data);
  }

  newMessageReceived(): Observable<ChatMessage> {
    return new Observable<ChatMessage>(observer => {
      this.socket.on('new message', (data) => {
        observer.next(data);
      });
    });
  }

  messageMetaUpdate(): Observable<ChatMessage> {
    return new Observable<ChatMessage>(observer => {
      this.socket.on('update message meta', (data) => {
        observer.next(data);
      });
    });
  }

  roomConnectedUsers(): Observable<User[]> {
    return new Observable<User[]>(observer => {
      this.socket.on('room users', (data) => {
        observer.next(data);
      });
    });
  }

  roomConnectedUsersByRoomId(groupId): Observable<User[]> {
    return new Observable<User[]>(observer => {
      console.log(`${groupId} connected users`);
      this.socket.on(`${groupId} connected users`, (data) => {
        console.log(data);
        observer.next(data);
      });
    });
  }

  knoknokWosZer(groupId) {
    this.socket.emit('toctocroom', groupId);
  }

  typing(data) {
    this.socket.emit('typing', data);
  }

  stopTyping(data) {
    this.socket.emit('stop typing', data);
  }

  receivedTyping(): Observable<{ isTyping: boolean, username: string, avatarUrl: string, hasAvatar: boolean}> {
    return new Observable<{ isTyping: boolean, username: string, avatarUrl: string, hasAvatar: boolean}>(observer => {
      this.socket.on('typing', (data) => {
        observer.next(data);
      });
    });
  }

  receivedStopTyping():  Observable<{ isTyping: boolean}> {
    return new Observable<{ isTyping: boolean}>(observer => {
      this.socket.on('stop typing', (data) => {
        observer.next(data);
      });
    });
  }
}
