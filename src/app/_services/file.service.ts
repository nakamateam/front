import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {File, User} from '../_models';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient) { }

  private defaultValue = -1;
  private groupFileId = new BehaviorSubject<number>(this.defaultValue);
  currentGroupFileId = this.groupFileId.asObservable();

  changeGroupFileId(fileId: number) {
    this.groupFileId.next(fileId);
  }

  getMyFiles(groupId): Observable<File[]> {
    return this.http.get<File[]>(environment.serverHttp + '/files/' + groupId);
  }

  removeFile(FileId, groupId): Observable<any> {
    return this.http.put<any>(environment.serverHttp + '/file', {fileId: FileId, wayId: groupId});
  }

  removeFileAndUpdateChat(FileId, groupId): Observable<any> {
    return this.http.put<any>(environment.serverHttp + '/chat-file', {fileId: FileId, wayId: groupId});
  }

  downloadMyFile(fileName): Observable<Blob> {
    return this.http.get(environment.serverHttp + '/file/' + fileName, { responseType: 'blob' });
  }

  getFileDatas(fileId, groupId): Observable<File> {
    return this.http.get<File>(environment.serverHttp + '/fileinfo/' + fileId + '/' + groupId);
  }

}
