import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Group, User} from '../_models';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  serverHttp: String = environment.serverHttp;

  constructor(private http: HttpClient) { }

  getWayDashboard(): Observable<Group[]> {
    return this.http.get<Group[]>(`${this.serverHttp}/dashboard`);
  }

  getNakamaWayTotalUsers(): Observable<number> {
    return this.http.get<number>(`${this.serverHttp}/total-users`);
  }

  getMongoDbStats(): Observable<any> {
    return this.http.get<any>(`${this.serverHttp}/mongo-stats`);
  }

  createNewnakama(newNakama): Observable<User> {
    return this.http.post<User>(`${this.serverHttp}/users`, newNakama);
  }

  getAllMembers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.serverHttp}/users`);
  }

  blockMember(member): Observable<User> {
    return this.http.put<User>(`${this.serverHttp}/user`, member);
  }

}
