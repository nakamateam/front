import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User, UserRole } from '../_models';
import {map} from 'rxjs/operators';
import {WebSocketService} from './web-socket.service';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient,
              private webSocketService: WebSocketService) {}

  keyUser = 'nakamaUser';
  ioToken = 'ioToken';
  mySelectedWay = 'myWay';
  me = new User();

  login(username: string, password: string) {
    const body = new HttpParams()
      .set('username', username)
      .set('password', password);
    return this.http.post<any>(environment.serverHttp + '/login', body.toString(), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          console.log(user);
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          this.me.role = User.parseUserRole(user.role);
          this.me.token = user.token;
          localStorage.setItem(this.keyUser, JSON.stringify(this.me));
          localStorage.setItem(this.ioToken, user.ioToken);
        }
        return user;
      }));
  }

  sendMeEmailToResetMyPw(email: string) {
    const body = new HttpParams()
      .set('email', email);
    return this.http.post<any>(environment.serverHttp + '/forgotpw', body.toString(), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  resetMyPw(token: string, newPassword: string, confirmPassword: string) {
    const body = new HttpParams()
      .set('token', token)
      .set('newPassword', newPassword)
      .set('confirmPassword', confirmPassword);
    return this.http.post<any>(environment.serverHttp + '/resetpw', body.toString(), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    });
  }

  logout() {
    // remove user from local storage to log user out
    this.webSocketService.closeSocket();
    localStorage.removeItem(this.keyUser);
    localStorage.removeItem(this.ioToken);
    localStorage.removeItem(this.mySelectedWay);
  }

  getUser(): User {
    return JSON.parse(localStorage.getItem(this.keyUser));
  }

  getIo(): string {
    return localStorage.getItem(this.ioToken);
  }

    isAuthenticated() {
    const currentUser = this.getUser();
    const currentIo = this.getIo();
    return (currentUser && currentUser.token && currentIo);
  }

  isAdmin(): boolean {
    const currentUser = this.getUser();
    return (currentUser && (currentUser.role === UserRole.NAKAMADMIN));
  }


}
