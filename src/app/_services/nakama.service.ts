import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {IUserResponse, User} from '../_models';
import {filter, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NakamaService {

  constructor(private http: HttpClient) { }

  getNewNakamas(): Observable<User[]> {
    return this.http.get<User[]>(environment.serverHttp + '/newnakamas');
  }

  search(query) {
    return this.http.post<any>(environment.serverHttp + '/newnakamas', {query: query});
  }

  getMyAcceptedNakama() {
    return this.http.get<any>(environment.serverHttp + '/acceptednakamas');
  }

  sendNakamaRequest(nakamaId: object): Observable<any> {
    return this.http.put<any>(environment.serverHttp + '/sendrequest', { nakamaId: nakamaId });
  }

  acceptNakamaRequest(nakamaId: object): Observable<any> {
    return this.http.put<any>(environment.serverHttp + '/acceptrequest', { nakamaId: nakamaId });
  }

  getMyNakamas(): Observable<User[]> {
    return this.http.get<User[]>(environment.serverHttp + '/nakamas');
  }

  deleteMyNakama(nakamaId): Observable<any> {
    return this.http.put<any>(environment.serverHttp + '/deletenakama', { nakamaId: nakamaId });
  }

  cancelRequestAndUpdateNotif(nakamaId): Observable<any> {
    return this.http.put<any>(environment.serverHttp + '/cancelrequest', { nakamaId: nakamaId });
  }
}
