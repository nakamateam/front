export const environment = {
  production: true,
  server: 'https://nakamaway',
  portServer: '3000',
  serverHttp: 'https://nakamaway:3000'
};
