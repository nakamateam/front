export const environment = {
  production: false,
  server: 'http://localhost',
  portServer: '3000',
  serverHttp: 'http://localhost:3000',
  // serverHttp: 'http://192.168.1.14:3000',
  socket: {
    baseUrl: 'http://localhost:8000',
    // baseUrl: 'http://192.168.1.14:8000',
    opts: {}
  },
};
